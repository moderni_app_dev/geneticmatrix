"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var gesture_1 = require("ionic-angular/gestures/gesture");
var PressDirective = /** @class */ (function () {
    function PressDirective(el) {
        this.el = el.nativeElement;
    }
    PressDirective.prototype.ngOnInit = function () {
        this.pressGesture = new gesture_1.Gesture(this.el);
        this.pressGesture.listen();
        this.pressGesture.on('press', function (e) {
            console.log('pressed!!');
        });
        this.pinchGesture = new gesture_1.Gesture(this.el);
        this.pinchGesture.listen();
        this.pinchGesture.on('pinchin', function (e) {
            console.log('pinched');
        });
    };
    PressDirective.prototype.ngOnDestroy = function () {
        this.pressGesture.destroy();
    };
    PressDirective = __decorate([
        core_1.Directive({
            selector: '[longPress]' // Attribute selector
        }),
        __metadata("design:paramtypes", [Object])
    ], PressDirective);
    return PressDirective;
}());
exports.PressDirective = PressDirective;
//# sourceMappingURL=press.js.map