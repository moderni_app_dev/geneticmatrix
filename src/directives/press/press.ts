import {Directive, ElementRef, Input, OnInit, OnDestroy} from '@angular/core';
import {Gesture} from 'ionic-angular/gestures/gesture';


@Directive({
  selector: '[longPress]' // Attribute selector
})
export class PressDirective implements OnInit, OnDestroy {
  el: HTMLElement;
  pressGesture: Gesture;
  pinchGesture: Gesture;

  constructor(el: ElementRef) {
    this.el = el.nativeElement;
  }

  ngOnInit() {
    this.pressGesture = new Gesture(this.el);
    this.pressGesture.listen();
    this.pressGesture.on('press', e => {
      console.log('pressed!!');
    });

    this.pinchGesture = new Gesture(this.el);
    this.pinchGesture.listen();
    this.pinchGesture.on('pinchin',e => {
      console.log('pinched');
    })
  }

  ngOnDestroy() {
    this.pressGesture.destroy();
  }
}