"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var audio_video_1 = require("./../pages/audio-video/audio-video");
var my_account_1 = require("./../pages/my-account/my-account");
var test_1 = require("./../pages/test/test");
var saturn_return_chart_1 = require("./../pages/saturn-return-chart/saturn-return-chart");
var jupiter_return_chart_1 = require("./../pages/jupiter-return-chart/jupiter-return-chart");
var mars_return_chart_1 = require("./../pages/mars-return-chart/mars-return-chart");
var venus_return_chart_1 = require("./../pages/venus-return-chart/venus-return-chart");
var solar_return_chart_1 = require("./../pages/solar-return-chart/solar-return-chart");
var lunar_return_chart_1 = require("./../pages/lunar-return-chart/lunar-return-chart");
var chiron_return_chart_1 = require("./../pages/chiron-return-chart/chiron-return-chart");
var uranus_return_chart_1 = require("./../pages/uranus-return-chart/uranus-return-chart");
var dream_composit_chart_1 = require("./../pages/dream-composit-chart/dream-composit-chart");
var dream_transit_chart_1 = require("./../pages/dream-transit-chart/dream-transit-chart");
var dream_chart_1 = require("./../pages/dream-chart/dream-chart");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var ionic_angular_1 = require("ionic-angular");
var app_component_1 = require("./app.component");
var http_1 = require("@angular/http");
var about_1 = require("../pages/about/about");
var contact_1 = require("../pages/contact/contact");
var home_1 = require("../pages/home/home");
var tabs_1 = require("../pages/tabs/tabs");
var login_1 = require("../pages/login/login");
var status_bar_1 = require("@ionic-native/status-bar");
var splash_screen_1 = require("@ionic-native/splash-screen");
var chart_provider_1 = require("../providers/chart-provider");
var splash_component_1 = require("../providers/splash-component");
var auth_service_1 = require("../providers/auth-service");
var url_provider_1 = require("../providers/url-provider");
var safepipe_1 = require("../providers/safepipe");
var forget_password_1 = require("../pages/forget-password/forget-password");
var add_chart_1 = require("../pages/add-chart/add-chart");
var foundation_chart_1 = require("../pages/foundation-chart/foundation-chart");
var foundation_transit_chart_1 = require("../pages/foundation-transit-chart/foundation-transit-chart");
var family_chart_1 = require("./../pages/family-chart/family-chart");
var connection_chart_1 = require("./../pages/connection-chart/connection-chart");
var connection_transit_chart_1 = require("./../pages/connection-transit-chart/connection-transit-chart");
var business_chart_1 = require("./../pages/business-chart/business-chart");
var define_1 = require("../providers/define");
var press_1 = require("../directives/press/press");
var charts_1 = require("../pages/charts/charts");
var animal_cat_chart_1 = require("./../pages/animal-cat-chart/animal-cat-chart");
var animal_dog_chart_1 = require("./../pages/animal-dog-chart/animal-dog-chart");
var animal_horse_chart_1 = require("./../pages/animal-horse-chart/animal-horse-chart");
var animal_rabbit_chart_1 = require("./../pages/animal-rabbit-chart/animal-rabbit-chart");
var animal_transit_cat_chart_1 = require("./../pages/animal-transit-cat-chart/animal-transit-cat-chart");
var animal_transit_dog_chart_1 = require("./../pages/animal-transit-dog-chart/animal-transit-dog-chart");
var animal_transit_horse_chart_1 = require("./../pages/animal-transit-horse-chart/animal-transit-horse-chart");
var animal_transit_rabbit_chart_1 = require("./../pages/animal-transit-rabbit-chart/animal-transit-rabbit-chart");
// import { Gesture } from 'ionic-angular';
var CustomHammerConfig = /** @class */ (function (_super) {
    __extends(CustomHammerConfig, _super);
    function CustomHammerConfig() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.overrides = {
            'rotate': { enable: true },
            'pinch': { enable: true }
        };
        return _this;
    }
    return CustomHammerConfig;
}(platform_browser_1.HammerGestureConfig));
exports.CustomHammerConfig = CustomHammerConfig;
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            declarations: [
                login_1.LoginPage,
                app_component_1.MyApp,
                about_1.AboutPage,
                contact_1.ContactPage,
                home_1.HomePage,
                tabs_1.TabsPage,
                safepipe_1.SafePipe,
                forget_password_1.ForgetPasswordPage,
                add_chart_1.AddChartPage,
                foundation_chart_1.FoundationChartPage,
                dream_chart_1.DreamChartPage,
                foundation_transit_chart_1.FoundationTransitChartPage,
                dream_transit_chart_1.DreamTransitChartPage,
                dream_composit_chart_1.DreamCompositChartPage,
                business_chart_1.BusinessChartPage,
                family_chart_1.FamilyChartPage,
                connection_chart_1.ConnectionChartPage,
                connection_transit_chart_1.ConnectionTransitChartPage,
                lunar_return_chart_1.LunarReturnChartPage,
                solar_return_chart_1.SolarReturnChartPage,
                venus_return_chart_1.VenusReturnChartPage,
                mars_return_chart_1.MarsReturnChartPage,
                jupiter_return_chart_1.JupiterReturnChartPage,
                saturn_return_chart_1.SaturnReturnChartPage,
                chiron_return_chart_1.ChironReturnChartPage,
                uranus_return_chart_1.UranusReturnChartPage,
                test_1.TestPage,
                press_1.PressDirective,
                charts_1.ChartsPage,
                my_account_1.MyAccountPage,
                audio_video_1.AudioVideoPage,
                animal_cat_chart_1.AnimalCatChartPage,
                animal_dog_chart_1.AnimalDogChartPage,
                animal_horse_chart_1.AnimalHorseChartPage,
                animal_rabbit_chart_1.AnimalRabbitChartPage,
                animal_transit_cat_chart_1.AnimalTransitCatChartPage,
                animal_transit_dog_chart_1.AnimalTransitDogChartPage,
                animal_transit_horse_chart_1.AnimalTransitHorseChartPage,
                animal_transit_rabbit_chart_1.AnimalTransitRabbitChartPage
            ],
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpModule,
                ionic_angular_1.IonicModule.forRoot(app_component_1.MyApp)
            ],
            bootstrap: [ionic_angular_1.IonicApp],
            entryComponents: [
                login_1.LoginPage,
                app_component_1.MyApp,
                about_1.AboutPage,
                contact_1.ContactPage,
                home_1.HomePage,
                tabs_1.TabsPage,
                forget_password_1.ForgetPasswordPage,
                add_chart_1.AddChartPage,
                foundation_chart_1.FoundationChartPage,
                dream_chart_1.DreamChartPage,
                foundation_transit_chart_1.FoundationTransitChartPage,
                dream_transit_chart_1.DreamTransitChartPage,
                dream_composit_chart_1.DreamCompositChartPage,
                family_chart_1.FamilyChartPage,
                connection_chart_1.ConnectionChartPage,
                connection_transit_chart_1.ConnectionTransitChartPage,
                business_chart_1.BusinessChartPage,
                lunar_return_chart_1.LunarReturnChartPage,
                solar_return_chart_1.SolarReturnChartPage,
                venus_return_chart_1.VenusReturnChartPage,
                mars_return_chart_1.MarsReturnChartPage,
                jupiter_return_chart_1.JupiterReturnChartPage,
                saturn_return_chart_1.SaturnReturnChartPage,
                chiron_return_chart_1.ChironReturnChartPage,
                uranus_return_chart_1.UranusReturnChartPage,
                test_1.TestPage,
                charts_1.ChartsPage,
                my_account_1.MyAccountPage,
                audio_video_1.AudioVideoPage,
                animal_cat_chart_1.AnimalCatChartPage,
                animal_dog_chart_1.AnimalDogChartPage,
                animal_horse_chart_1.AnimalHorseChartPage,
                animal_rabbit_chart_1.AnimalRabbitChartPage,
                animal_transit_cat_chart_1.AnimalTransitCatChartPage,
                animal_transit_dog_chart_1.AnimalTransitDogChartPage,
                animal_transit_horse_chart_1.AnimalTransitHorseChartPage,
                animal_transit_rabbit_chart_1.AnimalTransitRabbitChartPage
            ],
            providers: [
                status_bar_1.StatusBar,
                splash_screen_1.SplashScreen,
                chart_provider_1.ChartProvider,
                splash_component_1.SplashComponent,
                auth_service_1.AuthService,
                url_provider_1.UrlProvider,
                // Gesture,
                { provide: core_1.ErrorHandler, useClass: ionic_angular_1.IonicErrorHandler },
                define_1.DefineProvider,
                {
                    provide: platform_browser_1.HAMMER_GESTURE_CONFIG,
                    useClass: CustomHammerConfig
                }
            ]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map