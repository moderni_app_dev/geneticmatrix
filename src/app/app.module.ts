import { AudioVideoPage } from './../pages/audio-video/audio-video';
import { MyAccountPage } from './../pages/my-account/my-account';
import { TestPage } from './../pages/test/test';
import { SaturnReturnChartPage } from './../pages/saturn-return-chart/saturn-return-chart';
import { JupiterReturnChartPage } from './../pages/jupiter-return-chart/jupiter-return-chart';
import { MarsReturnChartPage } from './../pages/mars-return-chart/mars-return-chart';
import { VenusReturnChartPage } from './../pages/venus-return-chart/venus-return-chart';
import { SolarReturnChartPage } from './../pages/solar-return-chart/solar-return-chart';
import { LunarReturnChartPage } from './../pages/lunar-return-chart/lunar-return-chart';
import { ChironReturnChartPage } from './../pages/chiron-return-chart/chiron-return-chart';
import { UranusReturnChartPage } from './../pages/uranus-return-chart/uranus-return-chart';
import { UranusOppnChartPage } from './../pages/uranus-oppn-chart//uranus-oppn-chart';
import { DreamCompositChartPage } from './../pages/dream-composit-chart/dream-composit-chart';
import { DreamTransitChartPage } from './../pages/dream-transit-chart/dream-transit-chart';
import { DreamChartPage } from './../pages/dream-chart/dream-chart';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule, HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login'
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ChartProvider } from '../providers/chart-provider';
import { SplashComponent } from '../providers/splash-component';
import { AuthService } from '../providers/auth-service';
import { UrlProvider } from '../providers/url-provider';
import { SafePipe } from '../providers/safepipe';
import { ForgetPasswordPage } from '../pages/forget-password/forget-password';
import { AddChartPage } from '../pages/add-chart/add-chart';
import { FoundationChartPage } from '../pages/foundation-chart/foundation-chart';
import { FoundationTransitChartPage } from '../pages/foundation-transit-chart/foundation-transit-chart';
import { FamilyChartPage } from './../pages/family-chart/family-chart';
import { ConnectionChartPage } from './../pages/connection-chart/connection-chart';
import { ConnectionTransitChartPage } from './../pages/connection-transit-chart/connection-transit-chart';
import { BusinessChartPage } from './../pages/business-chart/business-chart';
import { DefineProvider } from '../providers/define';
import { PressDirective } from '../directives/press/press';
import { ChartsPage } from '../pages/charts/charts';
import { AnimalCatChartPage } from './../pages/animal-cat-chart/animal-cat-chart';
import { AnimalDogChartPage } from './../pages/animal-dog-chart/animal-dog-chart';
import { AnimalHorseChartPage } from './../pages/animal-horse-chart/animal-horse-chart';
import { AnimalRabbitChartPage } from './../pages/animal-rabbit-chart/animal-rabbit-chart';
import { AnimalTransitCatChartPage } from './../pages/animal-transit-cat-chart/animal-transit-cat-chart';
import { AnimalTransitDogChartPage } from './../pages/animal-transit-dog-chart/animal-transit-dog-chart';
import { AnimalTransitHorseChartPage } from './../pages/animal-transit-horse-chart/animal-transit-horse-chart';
import { AnimalTransitRabbitChartPage } from './../pages/animal-transit-rabbit-chart/animal-transit-rabbit-chart';
import { EditDreamPage } from './../pages/edit-dream/edit-dream';
import { EditDreamTransitPage } from './../pages/edit-dream-transit/edit-dream-transit';
import { EditDreamCompositePage } from './../pages/edit-dream-composite/edit-dream-composite';
import { EditConnectionPage } from './../pages/edit-connection/edit-connection';
import { EditBusinessChartPage } from './../pages/edit-business-chart/edit-business-chart';
import { EditFamilyChartPage } from './../pages/edit-family-chart/edit-family-chart';
import { EditLunarChartPage } from './../pages/edit-lunar-chart/edit-lunar-chart';
import { EditSolarChartPage } from './../pages/edit-solar-chart/edit-solar-chart';
import { EditVenusChartPage } from './../pages/edit-venus-chart/edit-venus-chart';
import { EditMarsChartPage } from './../pages/edit-mars-chart/edit-mars-chart';
import { EditJupiterChartPage } from './../pages/edit-jupiter-chart/edit-jupiter-chart';
import { EditSaturnChartPage } from './../pages/edit-saturn-chart/edit-saturn-chart';
import { EditChironChartPage } from './../pages/edit-chiron-chart/edit-chiron-chart';
import { EditUranusOppositionChartPage } from './../pages/edit-uranus-opposition-chart/edit-uranus-opposition-chart';
import { EditUranusChartPage } from './../pages/edit-uranus-chart/edit-uranus-chart';
import { EditAnimalCatChartPage } from './../pages/edit-animal-cat-chart/edit-animal-cat-chart';
import { EditAnimalDogChartPage } from './../pages/edit-animal-dog-chart/edit-animal-dog-chart';
import { EditAnimalHorseChartPage } from './../pages/edit-animal-horse-chart/edit-animal-horse-chart';
import { EditAnimalRabbitChartPage } from './../pages/edit-animal-rabbit-chart/edit-animal-rabbit-chart';
import { EditAnimalTransitDogChartPage } from './../pages/edit-animal-transit-dog-chart/edit-animal-transit-dog-chart';
import { EditAnimalTransitCatChartPage } from './../pages/edit-animal-transit-cat-chart/edit-animal-transit-cat-chart';
import { EditAnimalTransitHorseChartPage } from './../pages/edit-animal-transit-horse-chart/edit-animal-transit-horse-chart';
import { EditAnimalTransitRabbitChartPage } from './../pages/edit-animal-transit-rabbit-chart/edit-animal-transit-rabbit-chart';
import { EditFoundationChartPage } from './../pages/edit-foundation-chart/edit-foundation-chart';
import { CreateAccountPage } from './../pages/create-account/create-account';
import { SlideShowPage } from './../pages/slide-show/slide-show';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { LocationProvider } from '../providers/location/location';
import { SelectSearchableModule } from '../components/select/select-module';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';




// import { Gesture } from 'ionic-angular';

export class CustomHammerConfig extends HammerGestureConfig {
    overrides = {
        'rotate': { enable: true }, //rotate is disabled by default, so we need to enable it
        'pinch': { enable: true }
    }
}


@NgModule({
    declarations: [
        LoginPage,
        MyApp,
        AboutPage,
        ContactPage,
        HomePage,
        TabsPage,
        SafePipe,
        ForgetPasswordPage,
        AddChartPage,
        FoundationChartPage,
        DreamChartPage,
        FoundationTransitChartPage,
        DreamTransitChartPage,
        DreamCompositChartPage,
        BusinessChartPage,
        FamilyChartPage,
        ConnectionChartPage,
        ConnectionTransitChartPage,
        LunarReturnChartPage,
        SolarReturnChartPage,
        VenusReturnChartPage,
        MarsReturnChartPage,
        JupiterReturnChartPage,
        SaturnReturnChartPage,
        ChironReturnChartPage,
        UranusReturnChartPage,
        UranusOppnChartPage,
        TestPage,
        PressDirective,
        ChartsPage,
        MyAccountPage,
        AudioVideoPage,
        AnimalCatChartPage,
        AnimalDogChartPage,
        AnimalHorseChartPage,
        AnimalRabbitChartPage,
        AnimalTransitCatChartPage,
        AnimalTransitDogChartPage,
        AnimalTransitHorseChartPage,
        AnimalTransitRabbitChartPage,
        EditDreamPage,
        EditDreamTransitPage,
        EditDreamCompositePage,
        EditConnectionPage,
        EditBusinessChartPage,
        EditFamilyChartPage,
        EditLunarChartPage,
        EditSolarChartPage,
        EditVenusChartPage,
        EditMarsChartPage,
        EditJupiterChartPage,
        EditSaturnChartPage,
        EditChironChartPage,
        EditUranusOppositionChartPage,
        EditUranusChartPage,
        EditAnimalCatChartPage,
        EditAnimalDogChartPage,
        EditAnimalHorseChartPage,
        EditAnimalRabbitChartPage,
        EditAnimalTransitDogChartPage,
        EditAnimalTransitCatChartPage,
        EditAnimalTransitHorseChartPage,
        EditAnimalTransitRabbitChartPage,
        EditFoundationChartPage, CreateAccountPage, SlideShowPage,
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicModule.forRoot(MyApp), SelectSearchableModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        LoginPage,
        AboutPage,
        ContactPage,
        HomePage,
        TabsPage,
        ForgetPasswordPage,
        AddChartPage,
        FoundationChartPage,
        DreamChartPage,
        FoundationTransitChartPage,
        DreamTransitChartPage,
        DreamCompositChartPage,
        FamilyChartPage,
        ConnectionChartPage,
        ConnectionTransitChartPage,
        BusinessChartPage,
        LunarReturnChartPage,
        SolarReturnChartPage,
        VenusReturnChartPage,
        MarsReturnChartPage,
        JupiterReturnChartPage,
        SaturnReturnChartPage,
        ChironReturnChartPage,
        UranusReturnChartPage,
        UranusOppnChartPage,
        TestPage,
        ChartsPage,
        MyAccountPage,
        AudioVideoPage,
        AnimalCatChartPage,
        AnimalDogChartPage,
        AnimalHorseChartPage,
        AnimalRabbitChartPage,
        AnimalTransitCatChartPage,
        AnimalTransitDogChartPage,
        AnimalTransitHorseChartPage,
        AnimalTransitRabbitChartPage,
        EditDreamPage,
        EditDreamTransitPage,
        EditDreamCompositePage,
        EditConnectionPage,
        EditBusinessChartPage,
        EditFamilyChartPage,
        EditLunarChartPage,
        EditSolarChartPage,
        EditVenusChartPage,
        EditMarsChartPage,
        EditJupiterChartPage,
        EditSaturnChartPage,
        EditChironChartPage,
        EditUranusOppositionChartPage,
        EditUranusChartPage,
        EditAnimalCatChartPage,
        EditAnimalDogChartPage,
        EditAnimalHorseChartPage,
        EditAnimalRabbitChartPage,
        EditAnimalTransitDogChartPage,
        EditAnimalTransitCatChartPage,
        EditAnimalTransitHorseChartPage,
        EditAnimalTransitRabbitChartPage,
        EditFoundationChartPage, CreateAccountPage, SlideShowPage,
    ],
    providers: [
        StatusBar,
        SplashScreen,
        ChartProvider,
        SplashComponent,
        AuthService,
        UrlProvider,
        Geolocation, DeviceMotion,
        NativeGeocoder, ScreenOrientation,
        // Gesture,
        { provide: ErrorHandler, useClass: IonicErrorHandler },
        DefineProvider,
        {
            provide: HAMMER_GESTURE_CONFIG,
            useClass: CustomHammerConfig
        },
        LocationProvider
    ]
})
export class AppModule { }


