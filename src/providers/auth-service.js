"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var url_provider_1 = require("./url-provider");
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
var User = /** @class */ (function () {
    function User(response) {
        this.username = response.user_display_name;
        this.email = response.user_email;
        this.id = response.user_id;
        this.level = response.user_level;
        this.user_lvl_name = response.user_lvl_name;
        this.member_since = response.member_since;
    }
    return User;
}());
exports.User = User;
var AuthService = /** @class */ (function () {
    function AuthService(http, url) {
        // this.loginUrl = this.url.loginUrl;
        this.http = http;
        this.url = url;
    }
    AuthService.prototype.login = function (credentials) {
        var userObser;
        this.loginUrl = this.url.loginUrl + 'username=' + credentials.username + '&password=' + credentials.password;
        userObser = this.http.post(this.loginUrl, '')
            .map(function (response) { return response.json(); });
        return userObser;
    };
    /* Save jwt authentication token recieved from server.
     * This token will be used in each successive request to server.
     */
    AuthService.prototype.setAuthToken = function (token) {
        this.authToken = token;
    };
    AuthService.prototype.getAuthToken = function () {
        return this.authToken;
    };
    AuthService.prototype.setUser = function (response) {
        this.currentUser = new User(response);
    };
    AuthService.prototype.is_user_authenticated = function () {
        if (this.authToken != null) {
            return true;
        }
        else {
            return false;
        }
    };
    AuthService.prototype.getUserInfo = function () {
        return this.currentUser;
    };
    AuthService.prototype.getUserAccount = function (id) {
        var userObser = this.http.get('http://www.geneticmatrixtest.com/wp-json/wp/v2/getmyaccount/' + id)
            .map(function (response) { return response.json(); });
        return userObser;
    };
    AuthService.prototype.saveUserAccount = function (profile) {
        var dates = profile.birth_date.split("-");
        var times = profile.birth_time.split(":");
        var accountObser = this.http.get('http://www.geneticmatrixtest.com/wp-json/wp/v2/saveMyAccount/' + this.currentUser.level + '/' + this.currentUser.id + '/' + profile.first_name + '/' + profile.last_name + '/' + profile.username + '/' + profile.user_email + '/' + dates[0] + '/' + dates[1] + '/' + dates[2] + '/' + times[0] + '/' + times[1] + '/' + profile.country + '/' + profile.city)
            .map(function (response) { return response; });
        return accountObser;
    };
    AuthService.prototype.logout = function () {
        var _this = this;
        return Observable_1.Observable.create(function (observer) {
            _this.currentUser = null;
            observer.next(true);
            observer.complete();
        });
    };
    AuthService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [Object, url_provider_1.UrlProvider])
    ], AuthService);
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth-service.js.map