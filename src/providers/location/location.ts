import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { UrlProvider } from '../url-provider';

@Injectable()
export class LocationProvider {
  urlGeocoder: string;
  getUtc: string;
  serverUrl: string;
  constructor(public http: Http, public urlProvider: UrlProvider) {
    this.serverUrl = this.urlProvider.serverUrl;
    this.urlGeocoder = this.urlProvider.urlGeocoder;
    this.getUtc = this.urlProvider.getUtc;
  }
  public getlocation(city, state, country, country_code) {
    var result: any;
    result = this.http.get(this.urlGeocoder + city + ',' + state + ',' + country + '&key=AIzaSyBxk_YhBWkjIil5i0c9zcEFSEuw7v8ys7s&components=country:' + country_code)
      .map(response => response.json());
    return result;
  }

  public utcTime(birthdate, birthtime, lat, long, state_code_short, country_code, code) {
    var result: any;
    result = this.http.get(this.serverUrl + this.getUtc + birthdate + '/' + birthtime + '/' + lat + '/' + long + '/' + state_code_short + '/' + country_code + "/" + code).map(response => response.json());
    return result;
  }
}
