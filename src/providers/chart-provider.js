"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var url_provider_1 = require("./url-provider");
var ChartProvider = /** @class */ (function () {
    function ChartProvider(http, urlProvider) {
        this.http = http;
        this.urlProvider = urlProvider;
        this.serverUrl = this.urlProvider.serverUrl;
        this.getChartListUrl = this.urlProvider.getChartListUrl;
        this.getCountryListUrl = this.urlProvider.getCountryListUrl;
        this.getChartOptionsUrl = this.urlProvider.getChartOptionsUrl;
        this.checkCityUrl = this.urlProvider.checkCityUrl;
        this.getPeopleListUrl = this.urlProvider.getPeopleListUrl;
        this.getAnimalListUrl = this.urlProvider.getAnimalListUrl;
        this.createFoundationChartUrl = this.urlProvider.createFoundationChartUrl;
        this.createDreamChartUrl = this.urlProvider.createDreamChartUrl;
        this.createFoundationTransitChartUrl = this.urlProvider.createFoundationTransitChartUrl;
        this.createDreamTransitChartUrl = this.urlProvider.createDreamTransitChartUrl;
        this.createDreamCompositChartUrl = this.urlProvider.createDreamCompositChartUrl;
        this.createConnectionChartUrl = this.urlProvider.createConnectionChartUrl;
        this.createConnectionDreamTransitChartUrl = this.urlProvider.createConnectionDreamTransitChartUrl;
        this.createFamilyChartUrl = this.urlProvider.createFamilyChartUrl;
        this.createBusinessChartUrl = this.urlProvider.createBusinessChartUrl;
        this.createLunarReturnChartUrl = this.urlProvider.createLunarReturnChartUrl;
        this.createSolarReturnChartUrl = this.urlProvider.createSolarReturnChartUrl;
        this.createVenusReturnChartUrl = this.urlProvider.createVenusReturnChartUrl;
        this.createMarsReturnChartUrl = this.urlProvider.createMarsReturnChartUrl;
        this.createJupiterReturnChartUrl = this.urlProvider.createJupiterReturnChartUrl;
        this.createSaturnReturnChartUrl = this.urlProvider.createSaturnReturnChartUrl;
        this.createChironReturnChartUrl = this.urlProvider.createChironReturnChartUrl;
        this.createFoundationAnimalCatChartUrl = this.urlProvider.createFoundationAnimalCatChartUrl;
        this.createFoundationAnimalDogChartUrl = this.urlProvider.createFoundationAnimalDogChartUrl;
        this.createFoundationAnimalHorseChartUrl = this.urlProvider.createFoundationAnimalHorseChartUrl;
        this.createFoundationAnimalRabbitChartUrl = this.urlProvider.createFoundationAnimalRabbitChartUrl;
        this.createAnimalTransitCatChartUrl = this.urlProvider.createAnimalTransitCatChartUrl;
        this.createAnimalTransitDogChartUrl = this.urlProvider.createAnimalTransitDogChartUrl;
        this.createAnimalTransitHorseChartUrl = this.urlProvider.createAnimalTransitHorseChartUrl;
        this.createAnimalTransitRabbitChartUrl = this.urlProvider.createAnimalTransitRabbitChartUrl;
        this.getNoteUrl = this.urlProvider.getNoteUrl;
        this.getPreviousNoteUrl = this.urlProvider.getPreviousNoteUrl;
        this.saveNoteUrl = this.urlProvider.saveNoteUrl;
        this.delNoteUrl = this.urlProvider.delNoteUrl;
        this.setFavouriteUrl = this.urlProvider.setFavouriteUrl;
        this.getAudioChartListUrl = this.urlProvider.getAudioChartListUrl;
    }
    ChartProvider.prototype.getChart = function () {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + 'wp-content/themes/twentyten/assets/chart_svg.php?chart_id=83164&type=freeTransit&ver=0&lvl=3')
            .map(function (response) { return response; });
        return chartObserver;
    };
    ChartProvider.prototype.getChartList = function (user_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.getChartListUrl + user_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.getChartOptions = function (chart_id, level_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.getChartOptionsUrl + chart_id + '/' + level_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.getCountryList = function () {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.getCountryListUrl)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.checkCity = function (year, month, day, hours, minutes, country, city) {
        var cityObserver;
        cityObserver = this.http.get(this.serverUrl + this.checkCityUrl + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city)
            .map(function (response) { return response.json(); });
        return cityObserver;
    };
    ChartProvider.prototype.createFoundationChart = function (firstName, lastName, year, month, day, hours, minutes, country, city, majorCity, user_id, user_level) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createFoundationChartUrl + firstName + '/' + lastName + '/' + user_level + '/' + user_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.getPeopleList = function (user_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.getPeopleListUrl + user_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.getAnimalList = function (user_id, animalType) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.getAnimalList + user_id + '/' + animalType)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createDreamChart = function (p_id, client_level, user_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createDreamChartUrl + p_id + '/' + client_level + '/' + user_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createFoundationTransitChart = function (p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createFoundationTransitChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createDreamTransitChart = function (p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createDreamTransitChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createDreamCompositChart = function (p_id, client_level, user_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createDreamCompositChartUrl + p_id + '/' + client_level + '/' + user_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createLunarReturnChart = function (p_id, client_level, user_id, year, month) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createLunarReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + year + '/' + month)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createSolarReturnChart = function (p_id, client_level, user_id, year) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createSolarReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + year)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createVenusReturnChart = function (p_id, client_level, user_id, year) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createVenusReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + year)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createMarsReturnChart = function (p_id, client_level, user_id, year) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createMarsReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + year)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createJupiterReturnChart = function (p_id, client_level, user_id, return_cycle) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createJupiterReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + return_cycle)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createSaturnReturnChart = function (p_id, client_level, user_id, return_cycle) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createSaturnReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + return_cycle)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createChironReturnChart = function (p_id, client_level, user_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createChironReturnChartUrl + p_id + '/' + client_level + '/' + user_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createUranusReturnChart = function (p_id, client_level, user_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createChironReturnChartUrl + p_id + '/' + client_level + '/' + user_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createFoundationAnimalCatChart = function (animal_name, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createFoundationAnimalCatChartUrl + animal_name + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createFoundationAnimalDogChart = function (animal_name, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createFoundationAnimalDogChartUrl + animal_name + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createFoundationAnimalHorseChart = function (animal_name, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createFoundationAnimalHorseChartUrl + animal_name + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createFoundationAnimalRabbitChart = function (animal_name, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createFoundationAnimalRabbitChartUrl + animal_name + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createAnimalTransitCatChart = function (p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createAnimalTransitCatChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createAnimalTransitDogChart = function (p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createAnimalTransitDogChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createAnimalTransitHorseChart = function (p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createAnimalTransitHorseChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createAnimalTransitRabbitChart = function (p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createAnimalTransitRabbitChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createBusinessChart = function (p_id, client_level, user_id, chart_name) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createBusinessChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + chart_name)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createConnectionChart = function (p_id, client_level, user_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createConnectionChartUrl + p_id + '/' + client_level + '/' + user_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createConnectionTransitChart = function (p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createConnectionDreamTransitChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.createFamilyChart = function (p_id, client_level, user_id, chart_name) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.createFamilyChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + chart_name)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.showNote = function (client_level, user_id, chart_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.getNoteUrl + client_level + '/' + user_id + '/' + chart_id)
            .map(function (response) { return response; });
        return chartObserver;
    };
    ChartProvider.prototype.getPreviousNote = function (user_id, chart_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.getPreviousNoteUrl + user_id + '/' + chart_id)
            .map(function (response) { return response; });
        return chartObserver;
    };
    ChartProvider.prototype.saveNote = function (user_id, chart_id, note) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.saveNoteUrl + user_id + '/' + chart_id + '/' + note)
            .map(function (response) { return response; });
        return chartObserver;
    };
    ChartProvider.prototype.delNote = function (user_id, chart_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.delNoteUrl + user_id + '/' + chart_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider.prototype.setFavourite = function (client_id, chart_id) {
        var favObserver;
        favObserver = this.http.get(this.serverUrl + this.setFavouriteUrl + client_id + '/' + chart_id)
            .map(function (response) { return response.json(); });
        return favObserver;
    };
    ChartProvider.prototype.getAudioChartList = function (client_id) {
        var chartObserver;
        chartObserver = this.http.get(this.serverUrl + this.getAudioChartListUrl + client_id)
            .map(function (response) { return response.json(); });
        return chartObserver;
    };
    ChartProvider = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [Object, url_provider_1.UrlProvider])
    ], ChartProvider);
    return ChartProvider;
}());
exports.ChartProvider = ChartProvider;
//# sourceMappingURL=chart-provider.js.map