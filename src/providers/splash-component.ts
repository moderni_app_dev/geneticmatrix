import { AlertController, LoadingController, Loading } from 'ionic-angular';
import { Injectable } from '@angular/core';


@Injectable()
export class SplashComponent {

  loading: Loading;

  constructor(private alertCtrl: AlertController, private loadingCtrl: LoadingController) {

  }

  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
    setTimeout(() => {
      this.loading.dismiss();
    }, 3000);
  }
  showLoadingManual() {
    this.loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    this.loading.present();
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showSuccess(text) {
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);

  }
}
