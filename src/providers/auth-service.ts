import { UrlProvider } from './url-provider';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


export class User {
  username: string;
  email: string;
  id: string;
  level: string;
  user_lvl_name: string;
  member_since: string;

  constructor(response) {
    this.username = response.user_display_name;
    this.email = response.user_email;
    this.id = response.user_id;
    this.level = response.user_level;
    this.user_lvl_name = response.user_lvl_name;
    this.member_since = response.member_since;

  }
}

@Injectable()
export class AuthService {
  currentUser: User
  response: any;
  loginUrl: string;
  authToken: string;
  user_id: any;


  constructor(private http: Http, public url: UrlProvider) {
    // this.loginUrl = this.url.loginUrl;

  }

  public login(credentials) {

    var userObser: any;
    this.loginUrl = this.url.loginUrl + 'username=' + credentials.username + '&password=' + credentials.password;
    userObser = this.http.post(this.loginUrl, '')
      .map(response => response.json());
    return userObser;
  }


  /* Save jwt authentication token recieved from server. 
   * This token will be used in each successive request to server.
   */
  public setAuthToken(token) {

    this.authToken = token;

  }

  public getAuthToken() {
    return this.authToken;
  }

  public setUser(response) {
    this.currentUser = new User(response);
  }

  public is_user_authenticated() {
    if (this.authToken != null) {
      return true;
    }
    else {
      return false;
    }
  }

  public getUserInfo(): User {
    return this.currentUser;
  }

  public getUserAccount(id) {
    let userObser = this.http.get('http://www.geneticmatrixtest.com/wp-json/wp/v2/getmyaccount/' + id)
      .map(response => response.json());
    return userObser;
  }

  public saveUserAccount(profile) {
    var dates = profile.birth_date.split("-");
    var times = profile.birth_time.split(":");
    let accountObser = this.http.get('http://www.geneticmatrixtest.com/wp-json/wp/v2/saveMyAccount/' + this.currentUser.level + '/' + this.currentUser.id + '/' + profile.first_name + '/' + profile.last_name + '/' + profile.username + '/' + profile.user_email + '/' + dates[0] + '/' + dates[1] + '/' + dates[2] + '/' + times[0] + '/' + times[1] + '/' + profile.country + '/' + profile.city)
      .map(response => response);
    return accountObser;
  }

  public logout() {
    return Observable.create(observer => {
      this.currentUser = null;
      observer.next(true);
      observer.complete();
    });
  }


}