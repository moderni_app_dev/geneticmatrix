import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';


@Injectable()
export class UrlProvider {
  serverUrl: string = 'http://www.geneticmatrixtest.com/'
  loginUrl: string;
  myDirectoryUrl: string;
  getChartListUrl: string;
  getChartOptionsUrl: string;
  getCountryListUrl: string;
  checkCityUrl: string;
  getPeopleListUrl: string;
  getAnimalListUrl: string;
  createFoundationChartUrl: string;
  createDreamChartUrl: string;
  createFoundationTransitChartUrl: string;
  createDreamTransitChartUrl: string;
  createDreamCompositChartUrl: string;
  createConnectionChartUrl: string;
  createConnectionDreamTransitChartUrl: string;
  createFamilyChartUrl: string;
  createBusinessChartUrl: string;
  createLunarReturnChartUrl: string;
  createSolarReturnChartUrl: string;
  createVenusReturnChartUrl: string;
  createMarsReturnChartUrl: string;
  createJupiterReturnChartUrl: string;
  createSaturnReturnChartUrl: string;
  createChironReturnChartUrl: string;
  createUranusReturnChartUrl: string;
  createUranusOppnChartUrl: string;
  createFoundationAnimalCatChartUrl: string;
  createFoundationAnimalDogChartUrl: string;
  createFoundationAnimalHorseChartUrl: string;
  createFoundationAnimalRabbitChartUrl: string;
  createAnimalTransitCatChartUrl: string;
  createAnimalTransitDogChartUrl: string;
  createAnimalTransitHorseChartUrl: string;
  createAnimalTransitRabbitChartUrl: string;
  getNoteUrl: string;
  getPreviousNoteUrl: string;
  saveNoteUrl: string;
  delNoteUrl: string;
  setFavouriteUrl: string;
  editFoundationChartUrl: string;
  editDreamChartUrl: string;
  editFoundationTransitChartUrl: string;
  editDreamTransitChartUrl: string;
  editDreamCompositChartUrl: string;
  editConnectionChartUrl: string;
  editConnectionDreamTransitChartUrl: string;
  editFamilyChartUrl: string;
  editBusinessChartUrl: string;
  editLunarReturnChartUrl: string;
  editSolarReturnChartUrl: string;
  editVenusReturnChartUrl: string;
  editMarsReturnChartUrl: string;
  editJupiterReturnChartUrl: string;
  editSaturnReturnChartUrl: string;
  editChironReturnChartUrl: string;
  editUranusReturnChartUrl: string;
  editUranusOppnChartUrl: string;
  editFoundationAnimalCatChartUrl: string;
  editFoundationAnimalDogChartUrl: string;
  editFoundationAnimalHorseChartUrl: string;
  editFoundationAnimalRabbitChartUrl: string;
  editAnimalTransitCatChartUrl: string;
  editAnimalTransitDogChartUrl: string;
  editAnimalTransitHorseChartUrl: string;
  editAnimalTransitRabbitChartUrl: string;
  getAudioChartListUrl: string;
  getChartType: string;
  getUtc: string;
  urlGeocoder: string;
  getChartDetailUrl: string;
  checkEmailUseranmeUrl: string;
  userRegistrationUrl: string;


  constructor(public http: Http) {

    this.loginUrl = 'http://geneticmatrixtest.com/wp-json/jwt-auth/v1/token?';
    this.myDirectoryUrl = 'fr/wp-json/wp/v2/getclientdirectory/1';
    this.getChartListUrl = 'wp-json/wp/v2/getclientcharts/';
    this.getChartOptionsUrl = 'wp-json/wp/v2/getchartoptions/';
    this.getCountryListUrl = 'wp-json/wp/v2/getcountrylist/1';
    this.checkCityUrl = 'wp-json/wp/v2/checkcity/';
    this.getPeopleListUrl = 'wp-json/wp/v2/getPeopleList/';
    this.getAnimalListUrl = 'wp-json/wp/v2/getAnimalList/';
    this.createFoundationChartUrl = 'wp-json/wp/v2/createfoundationchart/';
    this.createDreamChartUrl = 'wp-json/wp/v2/createDreamchart/';
    this.createConnectionChartUrl = 'wp-json/wp/v2/createConnectionChart/';
    this.createConnectionDreamTransitChartUrl = 'wp-json/wp/v2/createConnectiomDreamTransitChart/';
    this.createFamilyChartUrl = 'wp-json/wp/v2/createFamilyChart/'
    this.createBusinessChartUrl = 'wp-json/wp/v2/createBusinessChart/';
    this.createFoundationTransitChartUrl = 'wp-json/wp/v2/createFoundationTransitChartForm/';
    this.createDreamTransitChartUrl = 'wp-json/wp/v2/createDreamTransitChartForm/';
    this.createDreamCompositChartUrl = 'wp-json/wp/v2/createDreamCompositeChart/';
    this.createLunarReturnChartUrl = 'wp-json/wp/v2/createLunarReturnChart/';
    this.createSolarReturnChartUrl = 'wp-json/wp/v2/createSolarReturnChart/';
    this.createVenusReturnChartUrl = 'wp-json/wp/v2/createVenusReturnChart/';
    this.createMarsReturnChartUrl = 'wp-json/wp/v2/createMarsReturnChart/';
    this.createJupiterReturnChartUrl = 'wp-json/wp/v2/createJupiterReturnChart/';
    this.createSaturnReturnChartUrl = 'wp-json/wp/v2/createSaturnReturnChart/';
    this.createChironReturnChartUrl = 'wp-json/wp/v2/createChironReturnChart/';
    this.createUranusReturnChartUrl = 'wp-json/wp/v2/createUranusReturnChart/';
    this.createUranusOppnChartUrl = 'wp-json/wp/v2/createUranusOppositionChart/';
    this.createFoundationAnimalCatChartUrl = 'wp-json/wp/v2/createFoundationAnimalCatChart/';
    this.createFoundationAnimalDogChartUrl = 'wp-json/wp/v2/createFoundationAnimalDogChart/';
    this.createFoundationAnimalHorseChartUrl = 'wp-json/wp/v2/createFoundationAnimalHorseChart/';
    this.createFoundationAnimalRabbitChartUrl = 'wp-json/wp/v2/createFoundationAnimalRabbitChart/';
    this.createAnimalTransitCatChartUrl = 'wp-json/wp/v2/createAnimalTransitCatChart/';
    this.createAnimalTransitDogChartUrl = 'wp-json/wp/v2/createAnimalTransitDogChart/';
    this.createAnimalTransitHorseChartUrl = 'wp-json/wp/v2/createAnimalTransitHorseChart/';
    this.createAnimalTransitRabbitChartUrl = 'wp-json/wp/v2/createAnimalTransitRabbitChart/';
    this.getNoteUrl = 'wp-json/wp/v2/showNote/';
    this.getPreviousNoteUrl = 'wp-json/wp/v2/getPreviousNote/';
    this.saveNoteUrl = 'wp-json/wp/v2/saveNote/';
    this.delNoteUrl = 'wp-json/wp/v2/delNote/';
    this.setFavouriteUrl = "wp-json/wp/v2/setChartFav/"
    this.editFoundationChartUrl = 'wp-json/wp/v2/editfoundationchart/';
    this.editDreamChartUrl = 'wp-json/wp/v2/editDreamchart/';
    this.editConnectionChartUrl = 'wp-json/wp/v2/editConnectionChart/';
    this.editConnectionDreamTransitChartUrl = 'wp-json/wp/v2/editConnectiomDreamTransitChart/';
    this.editFamilyChartUrl = 'wp-json/wp/v2/editFamilyChart/'
    this.editBusinessChartUrl = 'wp-json/wp/v2/editBusinessChart/';
    this.editFoundationTransitChartUrl = 'wp-json/wp/v2/editFoundationTransitChartForm/';
    this.editDreamTransitChartUrl = 'wp-json/wp/v2/editDreamTransitChart/';
    this.editDreamCompositChartUrl = 'wp-json/wp/v2/editDreamCompositeChart/';
    this.editLunarReturnChartUrl = 'wp-json/wp/v2/editLunarReturn/';
    this.editSolarReturnChartUrl = 'wp-json/wp/v2/editSolarReturn/';
    this.editVenusReturnChartUrl = 'wp-json/wp/v2/editVenusReturn/';
    this.editMarsReturnChartUrl = 'wp-json/wp/v2/editMarsReturn/';
    this.editJupiterReturnChartUrl = 'wp-json/wp/v2/editJupiterReturn/';
    this.editSaturnReturnChartUrl = 'wp-json/wp/v2/editSaturnReturn/';
    this.editChironReturnChartUrl = 'wp-json/wp/v2/editChironReturn/';
    this.editUranusReturnChartUrl = 'wp-json/wp/v2/editUranusReturn/';
    this.editUranusOppnChartUrl = 'wp-json/wp/v2/editUranusOppositon/';
    this.editFoundationAnimalCatChartUrl = 'wp-json/wp/v2/editFoundationAnimalCat/';
    this.editFoundationAnimalDogChartUrl = 'wp-json/wp/v2/editFoundationAnimalDog/';
    this.editFoundationAnimalHorseChartUrl = 'wp-json/wp/v2/editFoundationAnimalHorse/';
    this.editFoundationAnimalRabbitChartUrl = 'wp-json/wp/v2/editFoundationAnimalRabbit/';
    this.editAnimalTransitCatChartUrl = 'wp-json/wp/v2/editAnimalTransitCat/';
    this.editAnimalTransitDogChartUrl = 'wp-json/wp/v2/editAnimalTransitDog/';
    this.editAnimalTransitHorseChartUrl = 'wp-json/wp/v2/editAnimalTransitHorse/';
    this.editAnimalTransitRabbitChartUrl = 'wp-json/wp/v2/editAnimalTransitRabbit/';
    this.getAudioChartListUrl = 'wp-json/wp/v2/getaudiovideo/';
    this.getChartType = 'wp-json/wp/v2/getChartType/';
    this.getUtc = 'wp-json/wp/v2/getUtc/';
    this.urlGeocoder = 'https://maps.googleapis.com/maps/api/geocode/json?address=';
    this.getChartDetailUrl = "wp-json/wp/v2/getChartDetail/";
    this.checkEmailUseranmeUrl = "wp-json/wp/v2/checkEmailUseranme/";
    this.userRegistrationUrl = "wp-json/wp/v2/userRegistration/";

  }

}
