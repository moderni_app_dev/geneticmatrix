"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var UrlProvider = /** @class */ (function () {
    function UrlProvider(http) {
        this.http = http;
        this.serverUrl = 'http://www.geneticmatrixtest.com/';
        this.loginUrl = 'http://geneticmatrixtest.com/wp-json/jwt-auth/v1/token?';
        this.myDirectoryUrl = 'fr/wp-json/wp/v2/getclientdirectory/1';
        this.getChartListUrl = 'wp-json/wp/v2/getclientcharts/';
        this.getChartOptionsUrl = 'wp-json/wp/v2/getchartoptions/';
        this.getCountryListUrl = 'wp-json/wp/v2/getcountrylist/1';
        this.checkCityUrl = 'wp-json/wp/v2/checkcity/';
        this.getPeopleListUrl = 'wp-json/wp/v2/getPeopleList/';
        this.getAnimalListUrl = 'wp-json/wp/v2/getAnimalList/';
        this.createFoundationChartUrl = 'wp-json/wp/v2/createfoundationchart/';
        this.createDreamChartUrl = 'wp-json/wp/v2/createDreamchart/';
        this.createConnectionChartUrl = 'wp-json/wp/v2/createConnectionChart/';
        this.createConnectionDreamTransitChartUrl = 'wp-json/wp/v2/createConnectiomDreamTransitChart/';
        this.createFamilyChartUrl = 'wp-json/wp/v2/createFamilyChart/';
        this.createBusinessChartUrl = 'wp-json/wp/v2/createBusinessChart/';
        this.createFoundationTransitChartUrl = 'wp-json/wp/v2/createFoundationTransitChartForm/';
        this.createDreamTransitChartUrl = 'wp-json/wp/v2/createDreamTransitChartForm/';
        this.createDreamCompositChartUrl = 'wp-json/wp/v2/createDreamCompositeChart/';
        this.createLunarReturnChartUrl = 'wp-json/wp/v2/createLunarReturnChart/';
        this.createSolarReturnChartUrl = 'wp-json/wp/v2/createSolarReturnChart/';
        this.createVenusReturnChartUrl = 'wp-json/wp/v2/createVenusReturnChart/';
        this.createMarsReturnChartUrl = 'wp-json/wp/v2/createMarsReturnChart/';
        this.createJupiterReturnChartUrl = 'wp-json/wp/v2/createJupiterReturnChart/';
        this.createSaturnReturnChartUrl = 'wp-json/wp/v2/createSaturnReturnChart/';
        this.createChironReturnChartUrl = 'wp-json/wp/v2/createChironReturnChart/';
        this.createUranusReturnChartUrl = 'wp-json/wp/v2/createUranusReturnChart/';
        this.createFoundationAnimalCatChartUrl = 'wp-json/wp/v2/createFoundationAnimalCatChart/';
        this.createFoundationAnimalDogChartUrl = 'wp-json/wp/v2/createFoundationAnimalDogChart/';
        this.createFoundationAnimalHorseChartUrl = 'wp-json/wp/v2/createFoundationAnimalHorseChart/';
        this.createFoundationAnimalRabbitChartUrl = 'wp-json/wp/v2/createFoundationAnimalRabbitChart/';
        this.createAnimalTransitCatChartUrl = 'wp-json/wp/v2/createAnimalTransitCatChart/';
        this.createAnimalTransitDogChartUrl = 'wp-json/wp/v2/createAnimalTransitDogChart/';
        this.createAnimalTransitHorseChartUrl = 'wp-json/wp/v2/createAnimalTransitHorseChart/';
        this.createAnimalTransitRabbitChartUrl = 'wp-json/wp/v2/createAnimalTransitRabbitChart/';
        this.getNoteUrl = 'wp-json/wp/v2/showNote/';
        this.getPreviousNoteUrl = 'wp-json/wp/v2/getPreviousNote/';
        this.saveNoteUrl = 'wp-json/wp/v2/saveNote/';
        this.delNoteUrl = 'wp-json/wp/v2/delNote/';
        this.setFavouriteUrl = "wp-json/wp/v2/setChartFav/";
        this.editFoundationChartUrl = 'wp-json/wp/v2/editfoundationchart/';
        this.editDreamChartUrl = 'wp-json/wp/v2/editDreamchart/';
        this.editConnectionChartUrl = 'wp-json/wp/v2/editConnectionChart/';
        this.editConnectionDreamTransitChartUrl = 'wp-json/wp/v2/editConnectiomDreamTransitChart/';
        this.editFamilyChartUrl = 'wp-json/wp/v2/editFamilyChart/';
        this.editBusinessChartUrl = 'wp-json/wp/v2/editBusinessChart/';
        this.editFoundationTransitChartUrl = 'wp-json/wp/v2/editFoundationTransitChartForm/';
        this.editDreamTransitChartUrl = 'wp-json/wp/v2/editDreamTransitChartForm/';
        this.editDreamCompositChartUrl = 'wp-json/wp/v2/editDreamCompositeChart/';
        this.editLunarReturnChartUrl = 'wp-json/wp/v2/editLunarReturnChart/';
        this.editSolarReturnChartUrl = 'wp-json/wp/v2/editSolarReturnChart/';
        this.editVenusReturnChartUrl = 'wp-json/wp/v2/editVenusReturnChart/';
        this.editMarsReturnChartUrl = 'wp-json/wp/v2/editMarsReturnChart/';
        this.editJupiterReturnChartUrl = 'wp-json/wp/v2/editJupiterReturnChart/';
        this.editSaturnReturnChartUrl = 'wp-json/wp/v2/editSaturnReturnChart/';
        this.editChironReturnChartUrl = 'wp-json/wp/v2/editChironReturnChart/';
        this.editUranusReturnChartUrl = 'wp-json/wp/v2/editUranusReturnChart/';
        this.editFoundationAnimalCatChartUrl = 'wp-json/wp/v2/editFoundationAnimalCatChart/';
        this.editFoundationAnimalDogChartUrl = 'wp-json/wp/v2/editFoundationAnimalDogChart/';
        this.editFoundationAnimalHorseChartUrl = 'wp-json/wp/v2/editFoundationAnimalHorseChart/';
        this.editFoundationAnimalRabbitChartUrl = 'wp-json/wp/v2/editFoundationAnimalRabbitChart/';
        this.editAnimalTransitCatChartUrl = 'wp-json/wp/v2/editAnimalTransitCatChart/';
        this.editAnimalTransitDogChartUrl = 'wp-json/wp/v2/editAnimalTransitDogChart/';
        this.editAnimalTransitHorseChartUrl = 'wp-json/wp/v2/editAnimalTransitHorseChart/';
        this.editAnimalTransitRabbitChartUrl = 'wp-json/wp/v2/editAnimalTransitRabbitChart/';
        this.getAudioChartListUrl = 'wp-json/wp/v2/getaudiovideo/';
    }
    UrlProvider = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [Object])
    ], UrlProvider);
    return UrlProvider;
}());
exports.UrlProvider = UrlProvider;
//# sourceMappingURL=url-provider.js.map