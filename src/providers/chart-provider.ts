import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { UrlProvider } from './url-provider';


@Injectable()
export class ChartProvider {
	serverUrl: string;
	getChartListUrl: string;
	getChartOptionsUrl: string;
	getCountryListUrl: string;
	checkCityUrl: string;
	getPeopleListUrl: string;
	getAnimalListUrl: string;
	createFoundationChartUrl: string;
	editFoundationChartUrl: string;
	createDreamChartUrl: string;
	createBusinessChartUrl: string;
	createConnectionChartUrl: string;
	createConnectionDreamTransitChartUrl: string;
	createFamilyChartUrl: string;
	createFoundationTransitChartUrl: string;
	createDreamTransitChartUrl: string;
	createDreamCompositChartUrl: string;
	createLunarReturnChartUrl: string;
	createSolarReturnChartUrl: string;
	createVenusReturnChartUrl: string;
	createMarsReturnChartUrl: string;
	createJupiterReturnChartUrl: string;
	createSaturnReturnChartUrl: string;
	createChironReturnChartUrl: string;
	createUranusReturnChartUrl: string;
	createUranusOppnChartUrl: string;
	createFoundationAnimalCatChartUrl: string;
	createFoundationAnimalDogChartUrl: string;
	createFoundationAnimalHorseChartUrl: string;
	createFoundationAnimalRabbitChartUrl: string;
	createAnimalTransitCatChartUrl: string;
	createAnimalTransitDogChartUrl: string;
	createAnimalTransitHorseChartUrl: string;
	createAnimalTransitRabbitChartUrl: string;
	getNoteUrl: string;
	getPreviousNoteUrl: string;
	saveNoteUrl: string;
	delNoteUrl: string;
	setFavouriteUrl: string;
	getAudioChartListUrl: string;
	editDreamChartUrl: string;
	editDreamTransitChartUrl: string;
	editDreamCompositChartUrl: string;
	editConnectionChartUrl: string;
	editBusinessChartUrl: string;
	editFamilyChartUrl: string;
	editLunarReturnChartUrl: string;
	editVenusReturnChartUrl: string;
	editSolarReturnChartUrl: string;
	editMarsReturnChartUrl: string;
	editJupiterReturnChartUrl: string;
	editSaturnReturnChartUrl: string;
	editChironReturnChartUrl: string;
	editUranusOppnChartUrl: string;
	editUranusReturnChartUrl: string;
	editFoundationAnimalCatChartUrl: string;
	editFoundationAnimalHorseChartUrl: string;
	editFoundationAnimalDogChartUrl: string;
	editFoundationAnimalRabbitChartUrl: string;
	editAnimalTransitDogChartUrl: string;
	editAnimalTransitCatChartUrl: string;
	editAnimalTransitHorseChartUrl: string;
	editAnimalTransitRabbitChartUrl: string;

	getChartType: string;
	getChartDetailUrl: string;
	checkEmailUseranmeUrl: any;
	userRegistrationUrl: any;


	constructor(
		public http: Http,
		public urlProvider: UrlProvider
	) {
		this.serverUrl = this.urlProvider.serverUrl;
		this.getChartListUrl = this.urlProvider.getChartListUrl;
		this.getCountryListUrl = this.urlProvider.getCountryListUrl;
		this.getChartOptionsUrl = this.urlProvider.getChartOptionsUrl;
		this.checkCityUrl = this.urlProvider.checkCityUrl;
		this.getPeopleListUrl = this.urlProvider.getPeopleListUrl;
		this.getAnimalListUrl = this.urlProvider.getAnimalListUrl;
		this.createFoundationChartUrl = this.urlProvider.createFoundationChartUrl;
		this.createDreamChartUrl = this.urlProvider.createDreamChartUrl;
		this.createFoundationTransitChartUrl = this.urlProvider.createFoundationTransitChartUrl;
		this.createDreamTransitChartUrl = this.urlProvider.createDreamTransitChartUrl;
		this.createDreamCompositChartUrl = this.urlProvider.createDreamCompositChartUrl;
		this.createConnectionChartUrl = this.urlProvider.createConnectionChartUrl;
		this.createConnectionDreamTransitChartUrl = this.urlProvider.createConnectionDreamTransitChartUrl;
		this.createFamilyChartUrl = this.urlProvider.createFamilyChartUrl;
		this.createBusinessChartUrl = this.urlProvider.createBusinessChartUrl;
		this.createLunarReturnChartUrl = this.urlProvider.createLunarReturnChartUrl;
		this.createSolarReturnChartUrl = this.urlProvider.createSolarReturnChartUrl;
		this.createVenusReturnChartUrl = this.urlProvider.createVenusReturnChartUrl;
		this.createMarsReturnChartUrl = this.urlProvider.createMarsReturnChartUrl;
		this.createJupiterReturnChartUrl = this.urlProvider.createJupiterReturnChartUrl;
		this.createSaturnReturnChartUrl = this.urlProvider.createSaturnReturnChartUrl;
		this.createChironReturnChartUrl = this.urlProvider.createChironReturnChartUrl;
		this.createUranusOppnChartUrl = this.urlProvider.createUranusOppnChartUrl;
		this.createUranusReturnChartUrl = this.urlProvider.createUranusReturnChartUrl;
		this.createFoundationAnimalCatChartUrl = this.urlProvider.createFoundationAnimalCatChartUrl;
		this.createFoundationAnimalDogChartUrl = this.urlProvider.createFoundationAnimalDogChartUrl;
		this.createFoundationAnimalHorseChartUrl = this.urlProvider.createFoundationAnimalHorseChartUrl;
		this.createFoundationAnimalRabbitChartUrl = this.urlProvider.createFoundationAnimalRabbitChartUrl;
		this.createAnimalTransitCatChartUrl = this.urlProvider.createAnimalTransitCatChartUrl;
		this.createAnimalTransitDogChartUrl = this.urlProvider.createAnimalTransitDogChartUrl;
		this.createAnimalTransitHorseChartUrl = this.urlProvider.createAnimalTransitHorseChartUrl;
		this.createAnimalTransitRabbitChartUrl = this.urlProvider.createAnimalTransitRabbitChartUrl;
		this.getNoteUrl = this.urlProvider.getNoteUrl;
		this.getPreviousNoteUrl = this.urlProvider.getPreviousNoteUrl;
		this.saveNoteUrl = this.urlProvider.saveNoteUrl;
		this.delNoteUrl = this.urlProvider.delNoteUrl;
		this.setFavouriteUrl = this.urlProvider.setFavouriteUrl;
		this.getAudioChartListUrl = this.urlProvider.getAudioChartListUrl;
		this.editDreamChartUrl = this.urlProvider.editDreamChartUrl;
		this.editDreamTransitChartUrl = this.urlProvider.editDreamTransitChartUrl;
		this.editDreamCompositChartUrl = this.urlProvider.editDreamCompositChartUrl;
		this.editConnectionChartUrl = this.urlProvider.editConnectionChartUrl;
		this.editBusinessChartUrl = this.urlProvider.editBusinessChartUrl;
		this.editFamilyChartUrl = this.urlProvider.editFamilyChartUrl;
		this.editLunarReturnChartUrl = this.urlProvider.editLunarReturnChartUrl;
		this.editVenusReturnChartUrl = this.urlProvider.editVenusReturnChartUrl;
		this.editSolarReturnChartUrl = this.urlProvider.editSolarReturnChartUrl;
		this.editMarsReturnChartUrl = this.urlProvider.editMarsReturnChartUrl;
		this.editJupiterReturnChartUrl = this.urlProvider.editJupiterReturnChartUrl;
		this.editSaturnReturnChartUrl = this.urlProvider.editSaturnReturnChartUrl;
		this.editChironReturnChartUrl = this.urlProvider.editChironReturnChartUrl;
		this.editUranusOppnChartUrl = this.urlProvider.editUranusOppnChartUrl;
		this.editUranusReturnChartUrl = this.urlProvider.editUranusReturnChartUrl;
		this.editFoundationAnimalCatChartUrl = this.urlProvider.editFoundationAnimalCatChartUrl;
		this.editFoundationAnimalDogChartUrl = this.urlProvider.editFoundationAnimalDogChartUrl;
		this.editFoundationAnimalHorseChartUrl = this.urlProvider.editFoundationAnimalHorseChartUrl;
		this.editFoundationAnimalRabbitChartUrl = this.urlProvider.editFoundationAnimalRabbitChartUrl;
		this.editAnimalTransitDogChartUrl = this.urlProvider.editAnimalTransitDogChartUrl;
		this.editAnimalTransitCatChartUrl = this.urlProvider.editAnimalTransitCatChartUrl;
		this.editAnimalTransitHorseChartUrl = this.urlProvider.editAnimalTransitHorseChartUrl;
		this.editAnimalTransitRabbitChartUrl = this.urlProvider.editAnimalTransitRabbitChartUrl;
		this.editFoundationChartUrl = this.urlProvider.editFoundationChartUrl;

		this.getChartType = this.urlProvider.getChartType;
		this.getChartDetailUrl = this.urlProvider.getChartDetailUrl;
		this.checkEmailUseranmeUrl = this.urlProvider.checkEmailUseranmeUrl;
		this.userRegistrationUrl = this.urlProvider.userRegistrationUrl;
	}

	public getChart() {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + 'wp-content/themes/twentyten/assets/chart_svg.php?chart_id=83164&type=freeTransit&ver=0&lvl=3')
			.map(response => response);
		return chartObserver;
	}

	public getChartList(user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.getChartListUrl + user_id)
			.map(response => response.json());
		return chartObserver;

	}

	public getChartOptions(chart_id, level_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.getChartOptionsUrl + chart_id + '/' + level_id)
			.map(response => response.json());
		return chartObserver;
	}

	public getCountryList() {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.getCountryListUrl)
			.map(response => response.json());
		return chartObserver;
	}

	public checkCity(year, month, day, hours, minutes, country, city) {
		var cityObserver: any;

		cityObserver = this.http.get(this.serverUrl + this.checkCityUrl + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city)
			.map(response => response.json());
		return cityObserver;
	}

	public createFoundationChart(firstName, lastName, year, month, day, hours, minutes, country, state, state_code, city, utcDate, utcHour, utcMin, user_id, user_level) {
		var chartObserver: any;
		chartObserver = this.http.get(this.serverUrl + this.createFoundationChartUrl + firstName + '/' + lastName + '/' + user_level + '/' + user_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + state + '/' + state_code + '/' + utcDate + '/' + utcHour + '/' + utcMin)
			.map(response =>
				response.json());
		return chartObserver;
	}
	public editFoundationChart(firstName, lastName, year, month, day, hours, minutes, country, state, state_code, city, utcDate, utcHour, utcMin, user_id, user_level, chart_id) {
		var chartObserver: any;
		chartObserver = this.http.get(this.serverUrl + this.editFoundationChartUrl + firstName + '/' + lastName + '/' + user_level + '/' + user_id + '/' + chart_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + state + '/' + state_code + '/' + utcDate + '/' + utcHour + '/' + utcMin)
			.map(response =>
				response.json());
		return chartObserver;
	}

	public getPeopleList(user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.getPeopleListUrl + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public getAnimalList(user_id, animalType) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.getAnimalListUrl + user_id + '/' + animalType)
			// chartObserver = this.http.get('http://www.geneticmatrixtest.com/wp-json/wp/v2/getAnimalList/14454/r')
			.map(response => response.json());
		return chartObserver;
	}

	public createDreamChart(p_id, client_level, user_id) {
		var chartObserver: any;
		console.log(p_id);
		chartObserver = this.http.get(this.serverUrl + this.createDreamChartUrl + p_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public editDreamChart(p_id, chart_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editDreamChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id)
			.map(response => {
				response.json();
				console.log(response);
			});
		return chartObserver;
	}



	public createFoundationTransitChart(p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createFoundationTransitChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}

	public createDreamTransitChart(p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createDreamTransitChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}
	public editDreamTransitChart(p_id, chart_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editDreamTransitChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}

	public createDreamCompositChart(p_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createDreamCompositChartUrl + p_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public editDreamCompositChart(p_id, chart_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editDreamCompositChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}

	public createLunarReturnChart(p_id, client_level, user_id, year, month) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createLunarReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + year + '/' + month)
			.map(response => response.json());
		return chartObserver;
	}
	public editLunarReturn(p_id, chart_id, client_level, user_id, year, month) {
		var chartObserver: any;
		console.log(p_id);
		console.log(chart_id);
		console.log(client_level);
		console.log(user_id);
		console.log(year);
		console.log(month);

		chartObserver = this.http.get(this.serverUrl + this.editLunarReturnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + year + '/' + month)
			.map(response => response.json());


		return chartObserver;
	}

	public createSolarReturnChart(p_id, client_level, user_id, year) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createSolarReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + year)
			.map(response => response.json());
		return chartObserver;
	}
	public editSolarReturnChart(p_id, chart_id, client_level, user_id, year) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editSolarReturnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + year)
			.map(response => response.json());
		return chartObserver;
	}

	public createVenusReturnChart(p_id, client_level, user_id, year) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createVenusReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + year)
			.map(response => response.json());
		return chartObserver;
	}
	public editVenusReturnChart(p_id, chart_id, client_level, user_id, year) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editVenusReturnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + year)
			.map(response => response.json());
		return chartObserver;
	}

	public createMarsReturnChart(p_id, client_level, user_id, year) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createMarsReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + year)
			.map(response => response.json());
		return chartObserver;
	}
	public editMarsReturnChart(p_id, chart_id, client_level, user_id, year) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editMarsReturnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + year)
			.map(response => response.json());
		return chartObserver;
	}

	public createJupiterReturnChart(p_id, client_level, user_id, return_cycle) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createJupiterReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + return_cycle)
			.map(response => response.json());
		return chartObserver;
	}
	public editJupiterReturnChart(p_id, chart_id, client_level, user_id, return_cycle) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editJupiterReturnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + return_cycle)
			.map(response => response.json());
		return chartObserver;
	}

	public createSaturnReturnChart(p_id, client_level, user_id, return_cycle) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createSaturnReturnChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + return_cycle)
			.map(response => response.json());
		return chartObserver;
	}
	public editSaturnReturnChart(p_id, chart_id, client_level, user_id, return_cycle) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editSaturnReturnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + return_cycle)
			.map(response => response.json());
		return chartObserver;
	}

	public createChironReturnChart(p_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createChironReturnChartUrl + p_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public editChironReturnChart(p_id, chart_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editChironReturnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}

	public createUranusReturnChart(p_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createUranusReturnChartUrl + p_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public editUranusReturnChart(p_id, chart_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editUranusReturnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public createUranusOppnChart(p_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createUranusOppnChartUrl + p_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public editUranusOppnChart(p_id, chart_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editUranusOppnChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}

	public createFoundationAnimalCatChart(animal_name, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createFoundationAnimalCatChartUrl + animal_name + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
			.map(response => response.json());
		return chartObserver;
	}
	public editFoundationAnimalCatChart(animal_name, chart_id, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editFoundationAnimalCatChartUrl + animal_name + '/' + chart_id + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
			.map(response => response.json());
		return chartObserver;
	}

	public createFoundationAnimalDogChart(animal_name, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createFoundationAnimalDogChartUrl + animal_name + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
			.map(response => response.json());
		return chartObserver;
	}
	public editFoundationAnimalDogChart(animal_name, chart_id, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editFoundationAnimalDogChartUrl + animal_name + '/' + chart_id + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
			.map(response => response.json());
		return chartObserver;
	}

	public createFoundationAnimalHorseChart(animal_name, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createFoundationAnimalHorseChartUrl + animal_name + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
			.map(response => response.json());
		return chartObserver;
	}
	public editFoundationAnimalHorseChart(animal_name, chart_id, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editFoundationAnimalHorseChartUrl + animal_name + '/' + chart_id + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
			.map(response => response.json());
		return chartObserver;
	}

	public createFoundationAnimalRabbitChart(animal_name, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createFoundationAnimalRabbitChartUrl + animal_name + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
			.map(response => response.json());
		return chartObserver;
	}
	public editFoundationAnimalRabbitChart(animal_name, chart_id, client_level, p_id, year, month, day, hours, minutes, country, city, majorCity) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editFoundationAnimalRabbitChartUrl + animal_name + '/' + chart_id + '/' + client_level + '/' + p_id + '/' + year + '/' + month + '/' + day + '/' + hours + '/' + minutes + '/' + country + '/' + city + '/' + majorCity)
			.map(response => response.json());
		return chartObserver;
	}

	public createAnimalTransitCatChart(p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createAnimalTransitCatChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}
	public editAnimalTransitCatChart(p_id, client_level, chart_id, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editAnimalTransitCatChartUrl + p_id + '/' + client_level + '/' + chart_id + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}
	public createAnimalTransitDogChart(p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createAnimalTransitDogChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}
	public editAnimalTransitDogChart(p_id, chart_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editAnimalTransitDogChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}
	public createAnimalTransitHorseChart(p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createAnimalTransitHorseChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}
	public editAnimalTransitHorseChart(p_id, chart_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editAnimalTransitHorseChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}

	public createAnimalTransitRabbitChart(p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createAnimalTransitRabbitChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}
	public editAnimalTransitRabbitChart(p_id, client_level, chart_id, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editAnimalTransitRabbitChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}

	public createBusinessChart(p_id, client_level, user_id, chart_name) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createBusinessChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + chart_name)
			.map(response => response.json());
		return chartObserver;
	}
	public editBusinessChart(p_id, chart_id, client_level, user_id, chart_name) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editBusinessChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + chart_name)
			.map(response => response.json());
		return chartObserver;
	}

	public createConnectionChart(p_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createConnectionChartUrl + p_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public editConnectionChart(p_id, chart_id, client_level, user_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editConnectionChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id)
			.map(response => response.json());
		return chartObserver;
	}
	public createConnectionTransitChart(p_id, client_level, user_id, transit_now, year, month, day, hour, minutes) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createConnectionDreamTransitChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + transit_now + '/' + year + '/' + month + '/' + day + '/' + hour + '/' + minutes)
			.map(response => response.json());
		return chartObserver;
	}

	public createFamilyChart(p_id, client_level, user_id, chart_name) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.createFamilyChartUrl + p_id + '/' + client_level + '/' + user_id + '/' + chart_name)
			.map(response => response.json());
		return chartObserver;
	}
	public editFamilyChart(p_id, chart_id, client_level, user_id, chart_name) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.editFamilyChartUrl + p_id + '/' + chart_id + '/' + client_level + '/' + user_id + '/' + chart_name)
			.map(response => response.json());
		return chartObserver;
	}

	public showNote(client_level, user_id, chart_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.getNoteUrl + client_level + '/' + user_id + '/' + chart_id)
			.map(response => response);
		return chartObserver;
	}
	public getPreviousNote(user_id, chart_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.getPreviousNoteUrl + user_id + '/' + chart_id)
			.map(response => response);
		return chartObserver;
	}

	public saveNote(user_id, chart_id, note) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.saveNoteUrl + user_id + '/' + chart_id + '/' + note)
			.map(response => response);
		return chartObserver;
	}

	public delNote(user_id, chart_id) {
		var chartObserver: any;

		chartObserver = this.http.get(this.serverUrl + this.delNoteUrl + user_id + '/' + chart_id)
			.map(response => response.json());
		return chartObserver;
	}

	public setFavourite(client_id, chart_id) {
		var favObserver: any;
		favObserver = this.http.get(this.serverUrl + this.setFavouriteUrl + client_id + '/' + chart_id)
			.map(response => response.json());
		return favObserver;
	}

	public getAudioChartList(client_id) {
		var chartObserver: any;
		chartObserver = this.http.get(this.serverUrl + this.getAudioChartListUrl + client_id)
			.map(response => response.json());
		return chartObserver;
	}

	public getChartTypeAnimal(client_id) {
		var chartObserver: any;
		chartObserver = this.http.get(this.serverUrl + this.getChartType + client_id)
			.map(response => response.json());
		return chartObserver;

	}
	public getChartDetail(chart_id, chart_type) {
		var chartObserver: any;
		chartObserver = this.http.get(this.serverUrl + this.getChartDetailUrl + chart_id + '/' + chart_type)
			.map(response => response.json());
		return chartObserver;
	}
	public checkEmailUser(email, username) {
		var chartObserver: any;
		chartObserver = this.http.get(this.serverUrl + this.checkEmailUseranmeUrl + email + '/' + username)
			.map(response => response.json());
		return chartObserver;
	}
	public userRegistration(username, email, firstname, lastname, password) {
		var chartObserver: any;
		chartObserver = this.http.get(this.serverUrl + this.userRegistrationUrl + username + '/' + email + '/' + firstname + '/' + lastname + '/' + password)
			.map(response => response.json());
		return chartObserver;
	}


}