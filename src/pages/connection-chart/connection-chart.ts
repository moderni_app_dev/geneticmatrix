import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ChartsPage } from './../charts/charts';
import { DefineProvider } from './../../providers/define';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';


@IonicPage()
@Component({
  selector: 'page-connection-chart',
  templateUrl: 'connection-chart.html',
})
export class ConnectionChartPage {
  personList: any;
  selectedPerson: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public splash: SplashComponent,
    public chart: ChartProvider,
    public define: DefineProvider,
    public toastCtrl: ToastController
  ) {
    this.getPeopleList(this.auth.currentUser.id);
  }
  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }

  createConnectionChart() {
    let client_id = this.auth.currentUser.id;
    var user_level = this.auth.currentUser.level;
    if (this.selectedPerson.length != 2) {
      let toast = this.toastCtrl.create({
        message: 'Select 2 names',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    else {
      this.chart.createConnectionChart(this.selectedPerson, user_level, client_id)
        .subscribe(response => {
          if (response != false) {
            this.splash.loading.dismiss();
            this.splash.showSuccess("Chart Created successfully");
            this.navCtrl.setRoot(ChartsPage, response);
          }
          else {
            this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
          }
        })
    }
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad ConnectionChartPage');
  }

}
