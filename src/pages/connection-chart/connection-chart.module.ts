import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConnectionChartPage } from './connection-chart';

@NgModule({
  declarations: [
    ConnectionChartPage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectionChartPage),
  ],
  exports: [
    ConnectionChartPage
  ]
})
export class ConnectionChartPageModule {}
