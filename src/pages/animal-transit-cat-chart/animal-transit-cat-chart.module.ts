import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalTransitCatChartPage } from './animal-transit-cat-chart';

@NgModule({
  declarations: [
    AnimalTransitCatChartPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalTransitCatChartPage),
  ],
  exports: [
    AnimalTransitCatChartPage
  ]
})
export class AnimalTransitCatChartPageModule {}
