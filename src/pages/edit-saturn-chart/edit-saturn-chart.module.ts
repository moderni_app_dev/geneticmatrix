import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditSaturnChartPage } from './edit-saturn-chart';

@NgModule({
  declarations: [
    EditSaturnChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditSaturnChartPage),
  ],
})
export class EditSaturnChartPageModule {}
