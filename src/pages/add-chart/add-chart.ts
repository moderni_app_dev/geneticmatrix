import { SaturnReturnChartPage } from './../saturn-return-chart/saturn-return-chart';
import { JupiterReturnChartPage } from './../jupiter-return-chart/jupiter-return-chart';
import { JupitarReturnChartPage } from './../jupitar-return-chart/jupitar-return-chart';
import { MarsReturnChartPage } from './../mars-return-chart/mars-return-chart';
import { VenusReturnChartPage } from './../venus-return-chart/venus-return-chart';
import { SolarReturnChartPage } from './../solar-return-chart/solar-return-chart';
import { LunarReturnChartPage } from './../lunar-return-chart/lunar-return-chart';
import { ChironReturnChartPage } from './../chiron-return-chart/chiron-return-chart';
import { UranusReturnChartPage } from './../uranus-return-chart/uranus-return-chart';
import { UranusOppnChartPage } from './../uranus-oppn-chart/uranus-oppn-chart';
import { DreamCompositChartPage } from './../dream-composit-chart/dream-composit-chart';
import { DreamTransitChartPage } from './../dream-transit-chart/dream-transit-chart';
import { FamilyChartPage } from './../family-chart/family-chart';
import { ConnectionChartPage } from './../connection-chart/connection-chart';
import { ConnectionTransitChartPage } from './../connection-transit-chart/connection-transit-chart';
import { BusinessChartPage } from './../business-chart/business-chart';
import { FoundationTransitChartPage } from './../foundation-transit-chart/foundation-transit-chart';
import { AnimalCatChartPage } from './../animal-cat-chart/animal-cat-chart';
import { AnimalDogChartPage } from './../animal-dog-chart/animal-dog-chart';
import { AnimalHorseChartPage } from './../animal-horse-chart/animal-horse-chart';
import { AnimalRabbitChartPage } from './../animal-rabbit-chart/animal-rabbit-chart';
import { AnimalTransitCatChartPage } from './../animal-transit-cat-chart/animal-transit-cat-chart';
import { AnimalTransitDogChartPage } from './../animal-transit-dog-chart/animal-transit-dog-chart';
import { AnimalTransitHorseChartPage } from './../animal-transit-horse-chart/animal-transit-horse-chart';
import { AnimalTransitRabbitChartPage } from './../animal-transit-rabbit-chart/animal-transit-rabbit-chart';
import { DreamChartPage } from './../dream-chart/dream-chart';
import { FoundationChartPage } from './../foundation-chart/foundation-chart';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthService, User } from './../../providers/auth-service';



@Component({
  selector: 'page-add-chart',
  templateUrl: 'add-chart.html',
})
export class AddChartPage {
  currentUser: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, private alertCtrl: AlertController, ) {
    this.currentUser = this.auth.currentUser;


  }

  public foundationChartForm() {
    this.navCtrl.push(FoundationChartPage);
  }
  public foundationChartTransitForm() {
    if (this.currentUser.level > 1) {
      this.navCtrl.push(FoundationTransitChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  public dreamChartForm() {
    if (this.currentUser.level > 1) {
      this.navCtrl.push(DreamChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  public dreamTransitChartForm() {
    if (this.currentUser.level > 1) {
      this.navCtrl.push(DreamTransitChartPage);
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public connectionChartForm() {
    if (this.currentUser.level > 1) {
      this.navCtrl.push(ConnectionChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  public dreamCompositChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(DreamCompositChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public FamilyChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(FamilyChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public BusinessChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(BusinessChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public LunarReturnChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(LunarReturnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public SolarReturnChartForm() {
    if (this.currentUser.level > 1) {
      this.navCtrl.push(SolarReturnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public VenusReturnChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(VenusReturnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public MarsReturnChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(MarsReturnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public JupitarReturnChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(JupiterReturnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public SaturnReturnChartForm() {
    if (this.currentUser.level > 1) {
      this.navCtrl.push(SaturnReturnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public ChironReturnChartForm() {
    if (this.currentUser.level > 1) {
      this.navCtrl.push(ChironReturnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  public UranusReturnChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(UranusReturnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  public UranusOppnChartForm() {
    if (this.currentUser.level > 1) {
      this.navCtrl.push(UranusOppnChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  public CatChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(AnimalCatChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public DogChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(AnimalDogChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public HorseChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(AnimalHorseChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public RabbitChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(AnimalRabbitChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public CatTransitChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(AnimalTransitCatChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }

  }
  public DogTransitChartForm() {
    if (this.currentUser.level > 2) {

      this.navCtrl.push(AnimalTransitDogChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public HorseTransitChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(AnimalTransitHorseChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public RabbitTransitChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(AnimalTransitRabbitChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  public connectionTransitChartForm() {
    if (this.currentUser.level > 2) {
      this.navCtrl.push(ConnectionTransitChartPage);
    } else {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad AddChartPage');
  }

}

