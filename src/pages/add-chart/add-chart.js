"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var saturn_return_chart_1 = require("./../saturn-return-chart/saturn-return-chart");
var jupiter_return_chart_1 = require("./../jupiter-return-chart/jupiter-return-chart");
var mars_return_chart_1 = require("./../mars-return-chart/mars-return-chart");
var venus_return_chart_1 = require("./../venus-return-chart/venus-return-chart");
var solar_return_chart_1 = require("./../solar-return-chart/solar-return-chart");
var lunar_return_chart_1 = require("./../lunar-return-chart/lunar-return-chart");
var chiron_return_chart_1 = require("./../chiron-return-chart/chiron-return-chart");
var uranus_return_chart_1 = require("./../uranus-return-chart/uranus-return-chart");
var dream_composit_chart_1 = require("./../dream-composit-chart/dream-composit-chart");
var dream_transit_chart_1 = require("./../dream-transit-chart/dream-transit-chart");
var family_chart_1 = require("./../family-chart/family-chart");
var connection_chart_1 = require("./../connection-chart/connection-chart");
var connection_transit_chart_1 = require("./../connection-transit-chart/connection-transit-chart");
var business_chart_1 = require("./../business-chart/business-chart");
var foundation_transit_chart_1 = require("./../foundation-transit-chart/foundation-transit-chart");
var animal_cat_chart_1 = require("./../animal-cat-chart/animal-cat-chart");
var animal_dog_chart_1 = require("./../animal-dog-chart/animal-dog-chart");
var animal_horse_chart_1 = require("./../animal-horse-chart/animal-horse-chart");
var animal_rabbit_chart_1 = require("./../animal-rabbit-chart/animal-rabbit-chart");
var animal_transit_cat_chart_1 = require("./../animal-transit-cat-chart/animal-transit-cat-chart");
var animal_transit_dog_chart_1 = require("./../animal-transit-dog-chart/animal-transit-dog-chart");
var animal_transit_horse_chart_1 = require("./../animal-transit-horse-chart/animal-transit-horse-chart");
var animal_transit_rabbit_chart_1 = require("./../animal-transit-rabbit-chart/animal-transit-rabbit-chart");
var dream_chart_1 = require("./../dream-chart/dream-chart");
var foundation_chart_1 = require("./../foundation-chart/foundation-chart");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var AddChartPage = /** @class */ (function () {
    function AddChartPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    AddChartPage.prototype.foundationChartForm = function () {
        this.navCtrl.push(foundation_chart_1.FoundationChartPage);
    };
    AddChartPage.prototype.dreamChartForm = function () {
        this.navCtrl.push(dream_chart_1.DreamChartPage);
    };
    AddChartPage.prototype.foundationChartTransitForm = function () {
        this.navCtrl.push(foundation_transit_chart_1.FoundationTransitChartPage);
    };
    AddChartPage.prototype.dreamTransitChartForm = function () {
        this.navCtrl.push(dream_transit_chart_1.DreamTransitChartPage);
    };
    AddChartPage.prototype.dreamCompositChartForm = function () {
        this.navCtrl.push(dream_composit_chart_1.DreamCompositChartPage);
    };
    AddChartPage.prototype.BusinessChartForm = function () {
        this.navCtrl.push(business_chart_1.BusinessChartPage);
    };
    AddChartPage.prototype.LunarReturnChartForm = function () {
        this.navCtrl.push(lunar_return_chart_1.LunarReturnChartPage);
    };
    AddChartPage.prototype.SolarReturnChartForm = function () {
        this.navCtrl.push(solar_return_chart_1.SolarReturnChartPage);
    };
    AddChartPage.prototype.VenusReturnChartForm = function () {
        this.navCtrl.push(venus_return_chart_1.VenusReturnChartPage);
    };
    AddChartPage.prototype.MarsReturnChartForm = function () {
        this.navCtrl.push(mars_return_chart_1.MarsReturnChartPage);
    };
    AddChartPage.prototype.JupitarReturnChartForm = function () {
        this.navCtrl.push(jupiter_return_chart_1.JupiterReturnChartPage);
    };
    AddChartPage.prototype.SaturnReturnChartForm = function () {
        this.navCtrl.push(saturn_return_chart_1.SaturnReturnChartPage);
    };
    AddChartPage.prototype.ChironReturnChartForm = function () {
        this.navCtrl.push(chiron_return_chart_1.ChironReturnChartPage);
    };
    AddChartPage.prototype.UranusReturnChartForm = function () {
        this.navCtrl.push(uranus_return_chart_1.UranusReturnChartPage);
    };
    AddChartPage.prototype.CatChartForm = function () {
        this.navCtrl.push(animal_cat_chart_1.AnimalCatChartPage);
    };
    AddChartPage.prototype.DogChartForm = function () {
        this.navCtrl.push(animal_dog_chart_1.AnimalDogChartPage);
    };
    AddChartPage.prototype.HorseChartForm = function () {
        this.navCtrl.push(animal_horse_chart_1.AnimalHorseChartPage);
    };
    AddChartPage.prototype.RabbitChartForm = function () {
        this.navCtrl.push(animal_rabbit_chart_1.AnimalRabbitChartPage);
    };
    AddChartPage.prototype.CatTransitChartForm = function () {
        this.navCtrl.push(animal_transit_cat_chart_1.AnimalTransitCatChartPage);
    };
    AddChartPage.prototype.DogTransitChartForm = function () {
        this.navCtrl.push(animal_transit_dog_chart_1.AnimalTransitDogChartPage);
    };
    AddChartPage.prototype.HorseTransitChartForm = function () {
        this.navCtrl.push(animal_transit_horse_chart_1.AnimalTransitHorseChartPage);
    };
    AddChartPage.prototype.RabbitTransitChartForm = function () {
        this.navCtrl.push(animal_transit_rabbit_chart_1.AnimalTransitRabbitChartPage);
    };
    AddChartPage.prototype.connectionChartForm = function () {
        this.navCtrl.push(connection_chart_1.ConnectionChartPage);
    };
    AddChartPage.prototype.connectionTransitChartForm = function () {
        this.navCtrl.push(connection_transit_chart_1.ConnectionTransitChartPage);
    };
    AddChartPage.prototype.FamilyChartForm = function () {
        this.navCtrl.push(family_chart_1.FamilyChartPage);
    };
    AddChartPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad AddChartPage');
    };
    AddChartPage = __decorate([
        core_1.Component({
            selector: 'page-add-chart',
            templateUrl: 'add-chart.html',
        }),
        __metadata("design:paramtypes", [Object, Object])
    ], AddChartPage);
    return AddChartPage;
}());
exports.AddChartPage = AddChartPage;
//# sourceMappingURL=add-chart.js.map