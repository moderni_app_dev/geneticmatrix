import { Component, ViewChild, Directive, ElementRef, HostListener, Input, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { FormControl, FormGroup, Validators, ValidatorFn, AbstractControl } from '@angular/forms';
import { AuthService } from './../../providers/auth-service';
import { SplashComponent } from './../../providers/splash-component';
import { ChartProvider } from './../../providers/chart-provider';
import { LoginPage } from '../login/login';





@IonicPage()
@Component({
  selector: 'page-create-account',

  templateUrl: 'create-account.html',
})
export class CreateAccountPage {
  item6: string = 'my-item';;
  item5: string = 'my-item';;
  item4: string = 'my-item';;
  item3: string = 'my-item';;
  item2: string = 'my-item';;
  item1: any = 'my-item';
  register: FormGroup;
  email: any = "";
  username: any = "";
  firstname: any;
  lastname: any;
  password: any;
  re_password: any;
  errorMsg: any;
  success: any;
  disabled: any = true;

  constructor(public splash: SplashComponent, public navCtrl: NavController, public navParams: NavParams, public chart: ChartProvider, private alertCtrl: AlertController) {

  }

  public change() {
    if (this.email) {
      this.item1 = "my-item_change";
    }
    else {
      this.item1 = "my-item"
    }
    if (this.username) {
      this.item2 = "my-item_change";
    }
    else {
      this.item2 = "my-item"
    }
    if (this.firstname) {
      this.item3 = "my-item_change";
    }
    else {
      this.item3 = "my-item"
    }
    if (this.lastname) {
      this.item4 = "my-item_change";
    }
    else {
      this.item4 = "my-item"
    }
    if (this.password) {
      this.item5 = "my-item_change";
    }
    else {
      this.item5 = "my-item"
    }
    if (this.re_password) {
      this.item6 = "my-item_change";
    }
    else {
      this.item6 = "my-item"
    }

  }
  ngOnInit() {
    this.register = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
      username: new FormControl('', [Validators.required]),
      firstname: new FormControl('', [Validators.required]),
      lastname: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required, Validators.minLength(5)]),
      re_password: new FormControl('', [Validators.required, this.equalto('password')]),
    });

  }
  equalto(field_name): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {

      let input = control.value;

      let isValid = control.root.value[field_name] == input
      if (!isValid)
        return { 'equalTo': { isValid } }
      else
        return null;
    };
  }
  public checkEmailUser() {

    if (this.username != "" && this.email != "") {
      this.chart.checkEmailUser(this.email, this.username).subscribe(
        response => {
          if (response.success == false) {
            this.success = response.success;
            this.errorMsg = response.msg;
          }
          else {
            this.errorMsg = "";
            // this.success = "";

          }
        })
    }
  }

  public createAccount() {
    this.splash.showLoadingManual();
    if (this.username != "" && this.email != "") {
      this.chart.checkEmailUser(this.email, this.username).subscribe(
        response => {
          if (response.success == false) {
            this.success = response.success;
            this.errorMsg = response.msg;
          }
          else {
            this.errorMsg = "";
            if (this.email && this.username && this.firstname && this.lastname && this.password && this.re_password && this.errorMsg == "") {
              this.chart.userRegistration(this.username, this.email, this.firstname, this.lastname, this.password).subscribe(response => {
                // console.log(response);
                this.splash.loading.dismiss();
                if (response == 'success') {
                  this.navCtrl.setRoot(LoginPage, { msg: "Please check your email account and click the link to activate your new GM Account." });
                } else {
                  let alert = this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Error While Registration.Please Try Again',
                    buttons: ['OK']
                  });
                  alert.present();
                }
              })

            }
          }
        })
    }


  }
  public go_back() {
    this.navCtrl.setRoot(LoginPage);

  }
}
