import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UranusReturnChartPage } from './uranus-return-chart';

@NgModule({
  declarations: [
    UranusReturnChartPage,
  ],
  imports: [
    IonicPageModule.forChild(UranusReturnChartPage),
  ],
  exports: [
    UranusReturnChartPage
  ]
})
export class UranusReturnChartPageModule {}
