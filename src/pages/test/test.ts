import { PressDirective } from './../../directives/press/press';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@Component({
  selector: 'page-test',
  templateUrl: 'test.html',

})
export class TestPage {
  angle: Number;
  transformStyle: String;
  title = 'Rotate Me!';
  eventDetails: any;

  constructor(public navCtrl: NavController) {
    // set default angle to 0deg
    this.angle = 0;
    this.transformStyle = "rotate(0deg)";
  }

  onRotation(event: any): void {
    this.angle = event.angle;
    this.transformStyle = "rotate(" + this.angle + "deg)";
    console.log("retated");
  }

  public pinchevent(event: any) {
    this.eventDetails = event;
  }

  public tapevent(event: any) {
    this.eventDetails = event;
    console.log(event);
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad TestPage');
  }

}
