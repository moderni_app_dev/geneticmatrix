"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var audio_video_1 = require("./../audio-video/audio-video");
var my_account_1 = require("./../my-account/my-account");
var charts_1 = require("./../charts/charts");
var add_chart_1 = require("./../add-chart/add-chart");
var splash_component_1 = require("./../../providers/splash-component");
var auth_service_1 = require("./../../providers/auth-service");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var chart_provider_1 = require("../../providers/chart-provider");
var login_1 = require("../login/login");
var platform_browser_1 = require("@angular/platform-browser");
var ionic_angular_2 = require("ionic-angular");
var HomePage = /** @class */ (function () {
    function HomePage(menuCtrl, platform, navCtrl, chart, sanitizer, auth, splash, navParams) {
        this.menuCtrl = menuCtrl;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.chart = chart;
        this.sanitizer = sanitizer;
        this.auth = auth;
        this.splash = splash;
        this.navParams = navParams;
        this.menuCtrl.enable(false, 'chartsMenu');
        this.menuCtrl.enable(true, 'mainMenu');
        this.getChartList(this.auth.currentUser.id);
        this.chartList = [];
        this.svgSource = '';
        this.chartWidth = 100;
    }
    HomePage.prototype.ionViewDidLoad = function () {
    };
    HomePage.prototype.getChartList = function (user_id) {
        var _this = this;
        var favourite = 0;
        this.chart.getChartList(user_id).subscribe(function (response) {
            _this.chartList = response;
            _this.chartList.forEach(function (element) {
                if (element.cs_favorite == 1) {
                    _this.getChart(element.client_id, element.cs_id, element.cs_type);
                    favourite = 1;
                }
            });
            if (favourite == 0) {
                _this.getChart(_this.chartList[0].client_id, _this.chartList[0].cs_id, _this.chartList[0].cs_type);
            }
        });
    };
    HomePage.prototype.getChart = function (client_id, chart_id, chart_type) {
        this.splash.showLoading();
        if (chart_type == 'freeChiron' || chart_type == 'freeUranus') {
            chart_type = 'freeCycle';
        }
        if (chart_type == 'freeMars' || chart_type == 'freeSolar') {
            chart_type = 'freeCycle';
        }
        if (chart_type == 'freeLunar' || chart_type == 'freeVenus' || chart_type == 'freeJupiter') {
            chart_type = 'freeCycle';
        }
        if (chart_type == 'freeSaturn' || chart_type == 'freeSaturn2' || chart_type == 'freeSaturn3') {
            chart_type = 'freeCycle';
        }
        this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=" + chart_id + "&type=" + chart_type + "&lvl=2";
    };
    HomePage.prototype.loadChartsPage = function () {
        this.navCtrl.setRoot(charts_1.ChartsPage);
    };
    HomePage.prototype.loadAddChartPage = function () {
        this.navCtrl.push(add_chart_1.AddChartPage);
    };
    HomePage.prototype.loadMyAccountPage = function () {
        this.navCtrl.push(my_account_1.MyAccountPage);
    };
    HomePage.prototype.loadAudioChartPage = function () {
        this.navCtrl.setRoot(audio_video_1.AudioVideoPage);
    };
    HomePage.prototype.logout = function () {
        this.navCtrl.setRoot(login_1.LoginPage);
    };
    HomePage.prototype.zoomIn = function () {
        if (this.chartWidth < 250) {
            this.chartWidth = this.chartWidth + 10;
        }
    };
    HomePage.prototype.zoomOut = function () {
        if (this.chartWidth > 100) {
            this.chartWidth = this.chartWidth - 10;
        }
    };
    HomePage = __decorate([
        core_1.Component({
            selector: 'page-home',
            templateUrl: 'home.html',
        }),
        __metadata("design:paramtypes", [Object, Object, Object, chart_provider_1.ChartProvider, Object, auth_service_1.AuthService, splash_component_1.SplashComponent, Object])
    ], HomePage);
    return HomePage;
}());
exports.HomePage = HomePage;
//# sourceMappingURL=home.js.map