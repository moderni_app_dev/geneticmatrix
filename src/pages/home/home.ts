import { AudioVideoPage } from './../audio-video/audio-video';
import { MyAccountPage } from './../my-account/my-account';
import { ChartsPage } from './../charts/charts';
import { ElementRef } from '@angular/core';
import { AddChartPage } from './../add-chart/add-chart';
import { SafePipe } from './../../providers/safepipe';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService, User } from './../../providers/auth-service';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, NavParams, AlertController } from 'ionic-angular';
import { ChartProvider } from '../../providers/chart-provider';
import { LoginPage } from '../login/login';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { MenuController } from 'ionic-angular';
import { FoundationChartPage } from '../foundation-chart/foundation-chart';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  chartList: any;
  svgSource: SafeResourceUrl;
  chartWidth: any;

  constructor(private alertCtrl: AlertController, public menuCtrl: MenuController, public platform: Platform, public navCtrl: NavController, public chart: ChartProvider, public sanitizer: DomSanitizer, public auth: AuthService, public splash: SplashComponent, public navParams: NavParams) {
    this.menuCtrl.enable(false, 'chartsMenu');
    this.menuCtrl.enable(true, 'mainMenu');

    this.getChartList(this.auth.currentUser.id);

    this.chartList = [];
    this.svgSource = '';
    this.chartWidth = 100;
  }

  ionViewDidLoad() {
  }


  public getChartList(user_id) {

    let favourite = 0;

    this.chart.getChartList(user_id).subscribe(
      response => {
        // if (response == '') {
        //   this.navCtrl.push(FoundationChartPage);
        // } else {
        this.chartList = response;

        this.chartList.forEach(element => {

          if (element.cs_favorite == 1) {
            this.getChart(element.client_id, element.cs_id, element.cs_type);
            favourite = 1;
          }
        });

        if (favourite == 0) {
          this.getChart(this.chartList[0].client_id, this.chartList[0].cs_id, this.chartList[0].cs_type);
        }
        // }
      }
    )
  }

  public getChart(client_id, chart_id, chart_type) {
    this.splash.showLoading();

    if (chart_type == 'freeChiron' || chart_type == 'freeUranus') {
      chart_type = 'freeCycle';
    }

    if (chart_type == 'freeMars' || chart_type == 'freeSolar') {
      chart_type = 'freeCycle';
    }

    if (chart_type == 'freeLunar' || chart_type == 'freeVenus' || chart_type == 'freeJupiter') {
      chart_type = 'freeCycle';
    }

    if (chart_type == 'freeSaturn' || chart_type == 'freeSaturn2' || chart_type == 'freeSaturn3') {
      chart_type = 'freeCycle';
    }

    this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=" + chart_id + "&type=" + chart_type + "&lvl=2";
  }

  public loadChartsPage() {
    this.navCtrl.setRoot(ChartsPage);
  }

  public loadAddChartPage() {
    this.navCtrl.push(AddChartPage);
  }

  public loadMyAccountPage() {
    this.navCtrl.push(MyAccountPage);
  }

  public loadAudioChartPage() {
    this.navCtrl.setRoot(AudioVideoPage);
  }

  public logout() {
    let alert = this.alertCtrl.create({
      title: 'Logout',
      message: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Logout',
          handler: () => {
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }

  public zoomIn() {
    if (this.chartWidth < 250) {
      this.chartWidth = this.chartWidth + 10;
    }
  }

  public zoomOut() {
    if (this.chartWidth > 100) {
      this.chartWidth = this.chartWidth - 10;
    }
  }


}
