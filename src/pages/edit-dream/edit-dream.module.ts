import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditDreamPage } from './edit-dream';

@NgModule({
  declarations: [
    EditDreamPage,
  ],
  imports: [
    IonicPageModule.forChild(EditDreamPage),
  ],
})
export class EditDreamPageModule {}
