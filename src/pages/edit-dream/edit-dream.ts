import { Component } from '@angular/core';
import { ChartsPage } from './../charts/charts';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SplashComponent } from './../../providers/splash-component';
import { ChartProvider } from './../../providers/chart-provider';
import { AuthService } from './../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-edit-dream',
  templateUrl: 'edit-dream.html',
})
export class EditDreamPage {
  personList: any;
  selectedPerson: any;
  currentChart: any;
  chart_type: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public splash: SplashComponent, public auth: AuthService, public chart: ChartProvider, ) {
    this.getPeopleList(this.auth.currentUser.id);
    this.currentChart = navParams.get("currentChart");
    this.chart_type = navParams.get("chart_type");

  }
  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }
  public editDreamChart() {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.splash.showLoading();
    this.chart.editDreamChart(this.selectedPerson, this.currentChart, user_level, user_id).subscribe(
      response => {
        console.log(response);
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Updated successfully");
          this.navCtrl.setRoot(ChartsPage, { chart_id: this.currentChart, chart_type: this.chart_type });
        }
        else {
          this.splash.showError("Failed to update chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }
}
