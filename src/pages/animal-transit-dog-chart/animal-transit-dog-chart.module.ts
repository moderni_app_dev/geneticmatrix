import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalTransitDogChartPage } from './animal-transit-dog-chart';

@NgModule({
  declarations: [
    AnimalTransitDogChartPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalTransitDogChartPage),
  ],
  exports: [
    AnimalTransitDogChartPage
  ]
})
export class AnimalTransitDogChartPageModule {}
