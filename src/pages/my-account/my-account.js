"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var auth_service_1 = require("./../../providers/auth-service");
var splash_component_1 = require("./../../providers/splash-component");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var MyAccountPage = /** @class */ (function () {
    function MyAccountPage(navCtrl, navParams, splash, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.splash = splash;
        this.auth = auth;
        this.getMyAccount();
        this.profile = {};
        this.password = {};
    }
    MyAccountPage.prototype.getMyAccount = function () {
        var _this = this;
        this.splash.showLoading();
        this.auth.getUserAccount(this.auth.currentUser.id).subscribe(function (response) {
            console.log(response);
            _this.profile = response;
            _this.splash.loading.dismiss();
            // console.log(this.profile);  
        });
    };
    MyAccountPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad MyAccountPage');
    };
    MyAccountPage.prototype.cancelUpdate = function () {
        this.navCtrl.pop();
    };
    MyAccountPage.prototype.updateMyAccount = function () {
        var _this = this;
        this.splash.showLoading();
        this.auth.saveUserAccount(this.profile).subscribe(function (response) {
            _this.splash.loading.dismiss();
            console.log(response);
        });
    };
    MyAccountPage = __decorate([
        core_1.Component({
            selector: 'page-my-account',
            templateUrl: 'my-account.html',
        }),
        __metadata("design:paramtypes", [Object, Object, splash_component_1.SplashComponent,
            auth_service_1.AuthService])
    ], MyAccountPage);
    return MyAccountPage;
}());
exports.MyAccountPage = MyAccountPage;
//# sourceMappingURL=my-account.js.map