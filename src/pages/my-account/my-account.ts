import { AuthService } from './../../providers/auth-service';
import { SplashComponent } from './../../providers/splash-component';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-my-account',
  templateUrl: 'my-account.html',
})
export class MyAccountPage {

  profile:any;
  password:any;



  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public splash:SplashComponent,
              public auth:AuthService
            ) {
    this.getMyAccount();
    this.profile={};
    this.password={};
  }

  public getMyAccount() {
    this.splash.showLoading();
    this.auth.getUserAccount(this.auth.currentUser.id).subscribe(
      response=>{
        console.log(response);
        this.profile=response;
        this.splash.loading.dismiss();
        // console.log(this.profile);  
      }
    )
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad MyAccountPage');
  }

  public cancelUpdate() {
    this.navCtrl.pop();
  }

  public updateMyAccount() {
    this.splash.showLoading();
    this.auth.saveUserAccount(this.profile).subscribe(
      response => {
        this.splash.loading.dismiss();
        console.log(response);
      }
    )
  }
}