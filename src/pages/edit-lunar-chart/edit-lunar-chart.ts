import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChartsPage } from './../charts/charts';
import { DefineProvider } from './../../providers/define';
import { HomePage } from './../home/home';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-edit-lunar-chart',
  templateUrl: 'edit-lunar-chart.html',
})
export class EditLunarChartPage {
  personList: any;
  selectedPerson: any;
  year: any;
  month: any;
  years: any;
  months: any;
  currentChart: any;
  chart_type: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public splash: SplashComponent, public chart: ChartProvider, public define: DefineProvider) {
    this.getPeopleList(this.auth.currentUser.id);
    this.years = this.define.getYears();
    this.months = this.define.getMonths();
    this.currentChart = navParams.get("currentChart");
    this.chart_type = navParams.get("chart_type");
  }

  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }
  public editLunarReturnChart() {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.splash.showLoading();
    // console.log(this.selectedPerson);
    // console.log(this.currentChart);
    // console.log(user_level);
    // console.log(user_id);
    // console.log(this.year);
    // console.log(this.month);
    this.chart.editLunarReturn(this.selectedPerson, this.currentChart, user_level, user_id, this.year, this.month).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, { chart_id: this.currentChart, chart_type: this.chart_type });
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }
}
