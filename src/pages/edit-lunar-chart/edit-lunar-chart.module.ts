import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditLunarChartPage } from './edit-lunar-chart';

@NgModule({
  declarations: [
    EditLunarChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditLunarChartPage),
  ],
})
export class EditLunarChartPageModule {}
