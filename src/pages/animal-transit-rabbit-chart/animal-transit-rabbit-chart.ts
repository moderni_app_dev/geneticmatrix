import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from './../home/home';
import { ChartsPage } from './../charts/charts';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-animal-transit-rabbit-chart',
  templateUrl: 'animal-transit-rabbit-chart.html',
})
export class AnimalTransitRabbitChartPage {
  animalList: any;
  animalType: any = 'r';
  selectedAnimal: any;
  transitNow: any;
  birthDate: String;
  birthTime: String;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public splash: SplashComponent,
    public chart: ChartProvider
  ) {
    this.getanimalList(this.auth.currentUser.id);
  }
  public getanimalList(user_id) {
    this.splash.showLoading();
    this.chart.getAnimalList(this.auth.currentUser.id, this.animalType).subscribe(
      response => {
        this.animalList = response;
      }
    )
  }

  public checkTransitNow() {

    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;


    this.splash.showLoading();

    if (this.transitNow == true) {
      this.animalTransitRabbitChart(this.selectedAnimal, user_level, user_id, 1, 0, 0, 0, 0, 0)
    }
    else {

      var dates = this.birthDate.split("-");
      var times = this.birthTime.split(":");
      this.animalTransitRabbitChart(this.selectedAnimal, user_level, user_id, 0, dates[0], dates[1], dates[2], times[0], times[1])
    }
  }

  public animalTransitRabbitChart(selectedAnimal, user_level, user_id, transitNow, year, month, day, hour, minutes) {

    this.chart.createAnimalTransitRabbitChart(selectedAnimal, user_level, user_id, transitNow, year, month, day, hour, minutes).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, response);
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AnimalTransitRabbitChartPage');
  }

}
