import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalTransitRabbitChartPage } from './animal-transit-rabbit-chart';

@NgModule({
  declarations: [
    AnimalTransitRabbitChartPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalTransitRabbitChartPage),
  ],
  exports: [
    AnimalTransitRabbitChartPage
  ]
})
export class AnimalTransitRabbitChartPageModule {}
