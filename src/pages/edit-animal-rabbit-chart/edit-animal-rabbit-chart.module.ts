import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAnimalRabbitChartPage } from './edit-animal-rabbit-chart';

@NgModule({
  declarations: [
    EditAnimalRabbitChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAnimalRabbitChartPage),
  ],
  exports: [
    EditAnimalRabbitChartPage
  ]
})
export class EditAnimalRabbitChartPageModule {}
