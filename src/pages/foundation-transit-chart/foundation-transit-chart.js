"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var charts_1 = require("./../charts/charts");
var chart_provider_1 = require("./../../providers/chart-provider");
var splash_component_1 = require("./../../providers/splash-component");
var auth_service_1 = require("./../../providers/auth-service");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var FoundationTransitChartPage = /** @class */ (function () {
    function FoundationTransitChartPage(navCtrl, navParams, auth, splash, chart) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.splash = splash;
        this.chart = chart;
        this.getPeopleList(this.auth.currentUser.id);
    }
    FoundationTransitChartPage.prototype.getPeopleList = function (user_id) {
        var _this = this;
        this.splash.showLoading();
        this.chart.getPeopleList(this.auth.currentUser.id).subscribe(function (response) {
            _this.personList = response;
        });
    };
    FoundationTransitChartPage.prototype.checkTransitNow = function () {
        var user_level = this.auth.currentUser.level;
        var user_id = this.auth.currentUser.id;
        var dates = this.birthDate.split("-");
        var times = this.birthTime.split(":");
        this.splash.showLoading();
        if (this.transitNow == true) {
            this.createFoundationTransitChart(this.selectedPerson, user_level, user_id, 1, 0, 0, 0, 0, 0);
        }
        else {
            this.createFoundationTransitChart(this.selectedPerson, user_level, user_id, 0, dates[0], dates[1], dates[2], times[0], times[1]);
        }
    };
    FoundationTransitChartPage.prototype.createFoundationTransitChart = function (selectedPerson, user_level, user_id, transitNow, year, month, day, hour, minutes) {
        var _this = this;
        this.chart.createFoundationTransitChart(selectedPerson, user_level, user_id, transitNow, year, month, day, hour, minutes).subscribe(function (response) {
            if (response != false) {
                _this.splash.loading.dismiss();
                _this.splash.showSuccess("Chart Created successfully");
                _this.navCtrl.setRoot(charts_1.ChartsPage, response);
            }
            else {
                _this.splash.showError("Failed to create chart. Make sure you are connected to internet.");
            }
        });
    };
    FoundationTransitChartPage.prototype.cancelCreate = function () {
        this.navCtrl.pop();
    };
    FoundationTransitChartPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad FoundationTransitChartPage');
    };
    FoundationTransitChartPage = __decorate([
        core_1.Component({
            selector: 'page-foundation-transit-chart',
            templateUrl: 'foundation-transit-chart.html',
        }),
        __metadata("design:paramtypes", [Object, Object, auth_service_1.AuthService, splash_component_1.SplashComponent, chart_provider_1.ChartProvider])
    ], FoundationTransitChartPage);
    return FoundationTransitChartPage;
}());
exports.FoundationTransitChartPage = FoundationTransitChartPage;
//# sourceMappingURL=foundation-transit-chart.js.map