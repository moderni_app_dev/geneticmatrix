import { ChartsPage } from './../charts/charts';
import { HomePage } from './../home/home';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-foundation-transit-chart',
  templateUrl: 'foundation-transit-chart.html',
})
export class FoundationTransitChartPage {

  personList: any;
  selectedPerson: any;
  transitNow: any;
  birthDate: String;
  birthTime: String;
  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public splash: SplashComponent, public chart: ChartProvider) {
    this.getPeopleList(this.auth.currentUser.id);
  }



  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }

  public checkTransitNow() {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;

    var dates = this.birthDate.split("-");
    var times = this.birthTime.split(":");

    this.splash.showLoading();

    if (this.transitNow == true) {
      this.createFoundationTransitChart(this.selectedPerson, user_level, user_id, 1, 0, 0, 0, 0, 0)
    }
    else {
      this.createFoundationTransitChart(this.selectedPerson, user_level, user_id, 0, dates[0], dates[1], dates[2], times[0], times[1])
    }
  }

  public createFoundationTransitChart(selectedPerson, user_level, user_id, transitNow, year, month, day, hour, minutes) {

    this.chart.createFoundationTransitChart(selectedPerson, user_level, user_id, transitNow, year, month, day, hour, minutes).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, response);
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad FoundationTransitChartPage');
  }
}