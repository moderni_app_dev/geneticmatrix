import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { HomePage } from './../home/home';
import { ElementRef } from '@angular/core';
import { AddChartPage } from './../add-chart/add-chart';
import { SafePipe } from './../../providers/safepipe';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService, User } from './../../providers/auth-service';
import { ChartProvider } from '../../providers/chart-provider';
import { LoginPage } from '../login/login';
import { MenuController } from 'ionic-angular';


@Component({
  selector: 'page-audio-video',
  templateUrl: 'audio-video.html',
})
export class AudioVideoPage {

  svgSource: SafeResourceUrl;
  chartList: any;
  myInput: any;
  searchChartList: any;
  noSearchResults: any;

  flag: number;
  chartOptions: any;
  currentChart: any;

  baseWidth: any;
  chartWidth: any;
  splitMinWidth: any;

  zoomLevel: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public menuCtrl: MenuController,
    public platform: Platform,
    public chart: ChartProvider,
    public sanitizer: DomSanitizer,
    public auth: AuthService,
    public splash: SplashComponent
  ) {


    this.menuCtrl.enable(true, 'chartsMenu');
    this.menuCtrl.enable(false, 'mainMenu');

    this.flag = 1;
    // this.svgSource = '';
    this.chartList = [];


    this.getChartList(this.auth.currentUser.id);

    this.chartWidth = 100;
    this.noSearchResults = true;
    this.splitMinWidth = '768px';

  }
  public getChartList(user_id) {

    this.chart.getAudioChartList(user_id).subscribe(
      response => {

        this.chartList = response;
        console.log(this.chartList);
        this.searchChartList = this.chartList;
        if (this.searchChartList.length != 0) {
          this.noSearchResults = false;
        }

        this.getChart(this.chartList[0].chart_id, this.chartList[0].chart_type);

      }
    )
  }



  public onSearchInput($event) {
    this.searchChartList = Array();
    for (var i = 0; i < this.chartList.length; i++) {

      if (this.chartList[i].cs_name.toLowerCase().search(this.myInput.toLowerCase()) != -1) {
        this.noSearchResults = false;
        this.searchChartList.push(this.chartList[i]);
      }
    }

    if (this.searchChartList.length == 0) {
      this.noSearchResults = true;
    }
  }

  public getChart(chart_id, chart_type) {
    this.chartWidth = 100;
    this.currentChart = chart_id;
    this.splash.showLoading();
    this.menuCtrl.close();

    this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=" + chart_id + "&type=" + chart_type + "&lvl=2";
    // console.log(this.svgSource);
  }

  public addChart() {
    this.navCtrl.push(AddChartPage);
  }

  public zoomIn() {
    if (this.chartWidth < 250) {
      this.chartWidth = this.chartWidth + 10;
    }
  }

  public zoomOut() {
    if (this.chartWidth > 100) {
      this.chartWidth = this.chartWidth - 10;
    }
  }

  public resize() {
    if (this.flag == 1) {
      this.flag = 0;
    }
    else {
      this.flag = 1
    }
  }

  public setSplitPane() {
    if (this.flag == 1) {
      return false;
    }
    else {
      return true;
    }
  }

  public loadHomePage() {
    this.navCtrl.setRoot(HomePage);
  }

  public logout() {
    this.navCtrl.setRoot(LoginPage);
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad AudioVideoPage');
  }
}

// this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=2&type=talkFound&lvl=2";
