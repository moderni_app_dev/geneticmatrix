"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var platform_browser_1 = require("@angular/platform-browser");
var home_1 = require("./../home/home");
var add_chart_1 = require("./../add-chart/add-chart");
var splash_component_1 = require("./../../providers/splash-component");
var auth_service_1 = require("./../../providers/auth-service");
var chart_provider_1 = require("../../providers/chart-provider");
var login_1 = require("../login/login");
var ionic_angular_2 = require("ionic-angular");
var AudioVideoPage = /** @class */ (function () {
    function AudioVideoPage(navCtrl, navParams, menuCtrl, platform, chart, sanitizer, auth, splash) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.menuCtrl = menuCtrl;
        this.platform = platform;
        this.chart = chart;
        this.sanitizer = sanitizer;
        this.auth = auth;
        this.splash = splash;
        this.menuCtrl.enable(true, 'chartsMenu');
        this.menuCtrl.enable(false, 'mainMenu');
        this.flag = 1;
        // this.svgSource = '';
        this.chartList = [];
        this.getChartList(this.auth.currentUser.id);
        this.chartWidth = 100;
        this.noSearchResults = true;
        this.splitMinWidth = '768px';
    }
    AudioVideoPage.prototype.getChartList = function (user_id) {
        var _this = this;
        this.chart.getAudioChartList(user_id).subscribe(function (response) {
            _this.chartList = response;
            console.log(_this.chartList);
            _this.searchChartList = _this.chartList;
            if (_this.searchChartList.length != 0) {
                _this.noSearchResults = false;
            }
            _this.getChart(_this.chartList[0].chart_id, _this.chartList[0].chart_type);
        });
    };
    AudioVideoPage.prototype.onSearchInput = function ($event) {
        this.searchChartList = Array();
        for (var i = 0; i < this.chartList.length; i++) {
            if (this.chartList[i].cs_name.toLowerCase().search(this.myInput.toLowerCase()) != -1) {
                this.noSearchResults = false;
                this.searchChartList.push(this.chartList[i]);
            }
        }
        if (this.searchChartList.length == 0) {
            this.noSearchResults = true;
        }
    };
    AudioVideoPage.prototype.getChart = function (chart_id, chart_type) {
        this.chartWidth = 100;
        this.currentChart = chart_id;
        this.splash.showLoading();
        this.menuCtrl.close();
        this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=" + chart_id + "&type=" + chart_type + "&lvl=2";
        // console.log(this.svgSource);
    };
    AudioVideoPage.prototype.addChart = function () {
        this.navCtrl.push(add_chart_1.AddChartPage);
    };
    AudioVideoPage.prototype.zoomIn = function () {
        if (this.chartWidth < 250) {
            this.chartWidth = this.chartWidth + 10;
        }
    };
    AudioVideoPage.prototype.zoomOut = function () {
        if (this.chartWidth > 100) {
            this.chartWidth = this.chartWidth - 10;
        }
    };
    AudioVideoPage.prototype.resize = function () {
        if (this.flag == 1) {
            this.flag = 0;
        }
        else {
            this.flag = 1;
        }
    };
    AudioVideoPage.prototype.setSplitPane = function () {
        if (this.flag == 1) {
            return false;
        }
        else {
            return true;
        }
    };
    AudioVideoPage.prototype.loadHomePage = function () {
        this.navCtrl.setRoot(home_1.HomePage);
    };
    AudioVideoPage.prototype.logout = function () {
        this.navCtrl.setRoot(login_1.LoginPage);
    };
    AudioVideoPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad AudioVideoPage');
    };
    AudioVideoPage = __decorate([
        core_1.Component({
            selector: 'page-audio-video',
            templateUrl: 'audio-video.html',
        }),
        __metadata("design:paramtypes", [Object, Object, Object, Object, chart_provider_1.ChartProvider, Object, auth_service_1.AuthService,
            splash_component_1.SplashComponent])
    ], AudioVideoPage);
    return AudioVideoPage;
}());
exports.AudioVideoPage = AudioVideoPage;
// this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=2&type=talkFound&lvl=2";
//# sourceMappingURL=audio-video.js.map