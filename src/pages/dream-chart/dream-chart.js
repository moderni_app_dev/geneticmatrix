"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var charts_1 = require("./../charts/charts");
var splash_component_1 = require("./../../providers/splash-component");
var auth_service_1 = require("./../../providers/auth-service");
var chart_provider_1 = require("./../../providers/chart-provider");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var DreamChartPage = /** @class */ (function () {
    function DreamChartPage(navCtrl, navParams, chart, auth, splash) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.chart = chart;
        this.auth = auth;
        this.splash = splash;
        this.getPeopleList(this.auth.currentUser.id);
    }
    DreamChartPage.prototype.getPeopleList = function (user_id) {
        var _this = this;
        this.splash.showLoading();
        this.chart.getPeopleList(this.auth.currentUser.id).subscribe(function (response) {
            _this.personList = response;
        });
    };
    DreamChartPage.prototype.createDreamChart = function () {
        var _this = this;
        var user_level = this.auth.currentUser.level;
        var user_id = this.auth.currentUser.id;
        this.splash.showLoading();
        this.chart.createDreamChart(this.selectedPerson, user_level, user_id).subscribe(function (response) {
            if (response != false) {
                _this.splash.loading.dismiss();
                _this.splash.showSuccess("Chart Created successfully");
                _this.navCtrl.setRoot(charts_1.ChartsPage, response);
            }
            else {
                _this.splash.showError("Failed to create chart. Make sure you are connected to internet.");
            }
        });
    };
    DreamChartPage.prototype.cancelCreate = function () {
        this.navCtrl.pop();
    };
    DreamChartPage.prototype.ionViewDidLoad = function () {
    };
    DreamChartPage = __decorate([
        core_1.Component({
            selector: 'page-dream-chart',
            templateUrl: 'dream-chart.html',
        }),
        __metadata("design:paramtypes", [Object, Object, chart_provider_1.ChartProvider, auth_service_1.AuthService, splash_component_1.SplashComponent])
    ], DreamChartPage);
    return DreamChartPage;
}());
exports.DreamChartPage = DreamChartPage;
//# sourceMappingURL=dream-chart.js.map