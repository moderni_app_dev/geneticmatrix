import { ChartsPage } from './../charts/charts';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';
import { ChartProvider } from './../../providers/chart-provider';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from './../home/home';


@Component({
  selector: 'page-dream-chart',
  templateUrl: 'dream-chart.html',
})
export class DreamChartPage {

  personList: any;
  selectedPerson: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public chart: ChartProvider, public auth: AuthService, public splash: SplashComponent) {
    this.getPeopleList(this.auth.currentUser.id);
  }

  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }

  public createDreamChart() {
    console.log(this.selectedPerson);
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.splash.showLoading();
    this.chart.createDreamChart(this.selectedPerson, user_level, user_id).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, response);
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
  }

}
