import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditFamilyChartPage } from './edit-family-chart';

@NgModule({
  declarations: [
    EditFamilyChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditFamilyChartPage),
  ],
})
export class EditFamilyChartPageModule {}
