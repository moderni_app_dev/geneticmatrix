import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAnimalTransitRabbitChartPage } from './edit-animal-transit-rabbit-chart';

@NgModule({
  declarations: [
    EditAnimalTransitRabbitChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAnimalTransitRabbitChartPage),
  ],
  exports: [
    EditAnimalTransitRabbitChartPage
  ]
})
export class EditAnimalTransitRabbitChartPageModule {}
