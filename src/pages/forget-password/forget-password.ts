import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-forget-password',
  templateUrl: 'forget-password.html',
})
export class ForgetPasswordPage {


  user_email : string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  public forget_password_email() {
    console.log("submitted");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetPasswordPage');
  }

}
