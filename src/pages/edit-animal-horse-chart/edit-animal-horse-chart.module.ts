import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAnimalHorseChartPage } from './edit-animal-horse-chart';

@NgModule({
  declarations: [
    EditAnimalHorseChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAnimalHorseChartPage),
  ],
  exports: [
    EditAnimalHorseChartPage
  ]
})
export class EditAnimalHorseChartPageModule {}
