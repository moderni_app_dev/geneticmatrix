import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LoginPage } from '../login/login';

@IonicPage()
@Component({
  selector: 'page-slide-show',
  templateUrl: 'slide-show.html',
})
export class SlideShowPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  skip() {
    this.navCtrl.setRoot(LoginPage);
  }
  ionViewDidLoad() {
  }

}
