import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAnimalTransitHorseChartPage } from './edit-animal-transit-horse-chart';

@NgModule({
  declarations: [
    EditAnimalTransitHorseChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAnimalTransitHorseChartPage),
  ],
  exports: [
    EditAnimalTransitHorseChartPage
  ]
})
export class EditAnimalTransitHorseChartPageModule {}
