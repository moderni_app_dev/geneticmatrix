import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusinessChartPage } from './business-chart';

@NgModule({
  declarations: [
    BusinessChartPage,
  ],
  imports: [
    IonicPageModule.forChild(BusinessChartPage),
  ],
  exports: [
    BusinessChartPage
  ]
})
export class BusinessChartPageModule {}
