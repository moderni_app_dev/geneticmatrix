import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditConnectionPage } from './edit-connection';

@NgModule({
  declarations: [
    EditConnectionPage,
  ],
  imports: [
    IonicPageModule.forChild(EditConnectionPage),
  ],
})
export class EditConnectionPageModule {}
