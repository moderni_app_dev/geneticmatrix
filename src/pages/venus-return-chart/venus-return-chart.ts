import { ChartsPage } from './../charts/charts';
import { HomePage } from './../home/home';
import { DefineProvider } from './../../providers/define';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-venus-return-chart',
  templateUrl: 'venus-return-chart.html',
})
export class VenusReturnChartPage {
personList:any;
  selectedPerson:any;
  year:any;
  years:any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth:AuthService, public splash:SplashComponent, public chart:ChartProvider, public define:DefineProvider) {
      this.getPeopleList(this.auth.currentUser.id);
      this.years = this.define.getYears();
  }

  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }

  public createVenusReturnChart() {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.splash.showLoading();
    this.chart.createVenusReturnChart(this.selectedPerson,user_level,user_id,this.year).subscribe(
      response => {
        if(response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, response);
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.");
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad VenusReturnChartPage');
  }

}
