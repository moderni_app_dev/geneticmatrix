import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { HomePage } from './../home/home';
import { ChartsPage } from './../charts/charts';
import { AuthService } from './../../providers/auth-service';
import { SplashComponent } from './../../providers/splash-component';
import { ChartProvider } from './../../providers/chart-provider';


@IonicPage()
@Component({
  selector: 'page-animal-rabbit-chart',
  templateUrl: 'animal-rabbit-chart.html',
})
export class AnimalRabbitChartPage {
  countryList: any;
  animalName: string;
  birthDate: String;
  birthTime: String;
  country: String;
  city: String;
  majorCity: String;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public chart: ChartProvider,
    public splash: SplashComponent,
    public auth: AuthService
  ) {
    this.getCountryList();
  }
  public getCountryList() {
    this.splash.showLoading();
    this.chart.getCountryList().subscribe(
      list => {
        this.countryList = list
      }
    )
  }

  public checkCity() {

    var dates = this.birthDate.split("-");
    var times = this.birthTime.split(":");

    this.majorCity = this.majorCity.trim();

    this.chart.checkCity(dates[0], dates[1], dates[2], times[0], times[1], this.country, this.majorCity).subscribe(
      response => {
        if (response == 1) {
          this.splash.showLoading();
          this.dogFoundationChart(dates[0], dates[1], dates[2], times[0], times[1])
        }
        else {
          this.splash.showError("Nearest Major City not recognized.");
        }
      }
    );

  }

  public dogFoundationChart(year, month, day, hours, minutes) {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.chart.createFoundationAnimalRabbitChart(this.animalName, user_level, user_id, year, month, day, hours, minutes, this.country, this.city, this.majorCity).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, response);
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad AnimalRabbitChartPage');
  }

}
