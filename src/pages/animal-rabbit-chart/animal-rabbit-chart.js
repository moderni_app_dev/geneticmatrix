"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
// import { HomePage } from './../home/home';
var charts_1 = require("./../charts/charts");
var auth_service_1 = require("./../../providers/auth-service");
var splash_component_1 = require("./../../providers/splash-component");
var chart_provider_1 = require("./../../providers/chart-provider");
var AnimalRabbitChartPage = /** @class */ (function () {
    function AnimalRabbitChartPage(navCtrl, navParams, chart, splash, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.chart = chart;
        this.splash = splash;
        this.auth = auth;
        this.getCountryList();
    }
    AnimalRabbitChartPage.prototype.getCountryList = function () {
        var _this = this;
        this.splash.showLoading();
        this.chart.getCountryList().subscribe(function (list) {
            _this.countryList = list;
        });
    };
    AnimalRabbitChartPage.prototype.checkCity = function () {
        var _this = this;
        var dates = this.birthDate.split("-");
        var times = this.birthTime.split(":");
        this.majorCity = this.majorCity.trim();
        this.chart.checkCity(dates[0], dates[1], dates[2], times[0], times[1], this.country, this.majorCity).subscribe(function (response) {
            if (response == 1) {
                _this.splash.showLoading();
                _this.dogFoundationChart(dates[0], dates[1], dates[2], times[0], times[1]);
            }
            else {
                _this.splash.showError("Nearest Major City not recognized.");
            }
        });
    };
    AnimalRabbitChartPage.prototype.dogFoundationChart = function (year, month, day, hours, minutes) {
        var _this = this;
        var user_level = this.auth.currentUser.level;
        var user_id = this.auth.currentUser.id;
        this.chart.createFoundationAnimalDogChart(this.animalName, user_level, user_id, year, month, day, hours, minutes, this.country, this.city, this.majorCity).subscribe(function (response) {
            if (response != false) {
                _this.splash.loading.dismiss();
                _this.splash.showSuccess("Chart Created successfully");
                _this.navCtrl.setRoot(charts_1.ChartsPage, response);
            }
            else {
                _this.splash.showError("Failed to create chart. Make sure you are connected to internet.");
            }
        });
    };
    AnimalRabbitChartPage.prototype.cancelCreate = function () {
        this.navCtrl.pop();
    };
    AnimalRabbitChartPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AnimalRabbitChartPage');
    };
    AnimalRabbitChartPage = __decorate([
        ionic_angular_1.IonicPage(),
        core_1.Component({
            selector: 'page-animal-rabbit-chart',
            templateUrl: 'animal-rabbit-chart.html',
        }),
        __metadata("design:paramtypes", [Object, Object, chart_provider_1.ChartProvider,
            splash_component_1.SplashComponent,
            auth_service_1.AuthService])
    ], AnimalRabbitChartPage);
    return AnimalRabbitChartPage;
}());
exports.AnimalRabbitChartPage = AnimalRabbitChartPage;
//# sourceMappingURL=animal-rabbit-chart.js.map