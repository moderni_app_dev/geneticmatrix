import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalRabbitChartPage } from './animal-rabbit-chart';

@NgModule({
  declarations: [
    AnimalRabbitChartPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalRabbitChartPage),
  ],
  exports: [
    AnimalRabbitChartPage
  ]
})
export class AnimalRabbitChartPageModule {}
