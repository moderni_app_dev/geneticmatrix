import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalHorseChartPage } from './animal-horse-chart';

@NgModule({
  declarations: [
    AnimalHorseChartPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalHorseChartPage),
  ],
  exports: [
    AnimalHorseChartPage
  ]
})
export class AnimalHorseChartPageModule {}
