import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAnimalTransitDogChartPage } from './edit-animal-transit-dog-chart';

@NgModule({
  declarations: [
    EditAnimalTransitDogChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAnimalTransitDogChartPage),
  ],
  exports: [
    EditAnimalTransitDogChartPage
  ]
})
export class EditAnimalTransitDogChartPageModule {}
