import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { HomePage } from './../home/home';
import { ChartsPage } from './../charts/charts';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-edit-animal-transit-dog-chart',
  templateUrl: 'edit-animal-transit-dog-chart.html',
})
export class EditAnimalTransitDogChartPage {
  animalList: any;
  animalType: any = 'd';
  selectedAnimal: any;
  transitNow: any;
  birthDate: String;
  birthTime: String;
  currentChart: any;
  chart_type: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public splash: SplashComponent,
    public chart: ChartProvider
  ) {
    this.getanimalList(this.auth.currentUser.id);
    this.currentChart = navParams.get("currentChart");
    this.chart_type = navParams.get("chart_type");
  }
  public getanimalList(user_id) {
    this.splash.showLoading();
    this.chart.getAnimalList(this.auth.currentUser.id, this.animalType).subscribe(
      response => {
        this.animalList = response;
        console.log(this.animalList);
      }
    )
  }


  public checkTransitNow() {

    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;



    this.splash.showLoading();

    if (this.transitNow == true) {
      this.animalTransitDogChart(this.selectedAnimal, user_level, user_id, 1, 0, 0, 0, 0, 0)
    }
    else {
      var dates = this.birthDate.split("-");
      var times = this.birthTime.split(":");
      this.animalTransitDogChart(this.selectedAnimal, user_level, user_id, 0, dates[0], dates[1], dates[2], times[0], times[1])
    }
  }

  public animalTransitDogChart(selectedAnimal, user_level, user_id, transitNow, year, month, day, hour, minutes) {

    this.chart.editAnimalTransitDogChart(selectedAnimal, this.currentChart, user_level, user_id, transitNow, year, month, day, hour, minutes).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Update successfully");
          this.navCtrl.setRoot(ChartsPage, { chart_id: this.currentChart, chart_type: this.chart_type });

        }
        else {
          this.splash.showError("Failed to update chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }


}
