import { ChartsPage } from './../charts/charts';
import { HomePage } from './../home/home';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-dream-composit-chart',
  templateUrl: 'dream-composit-chart.html',
})
export class DreamCompositChartPage {

  personList: any;
  selectedPerson: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public splash: SplashComponent, public chart: ChartProvider) {
    this.getPeopleList(this.auth.currentUser.id);
  }

  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }

  public createDreamCompositChart() {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.splash.showLoading();
    this.chart.createDreamCompositChart(this.selectedPerson, user_level, user_id).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, response);
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad DreamCompositChartPage');
  }

}
