import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditDreamCompositePage } from './edit-dream-composite';

@NgModule({
  declarations: [
    EditDreamCompositePage,
  ],
  imports: [
    IonicPageModule.forChild(EditDreamCompositePage),
  ],
})
export class EditDreamCompositePageModule {}
