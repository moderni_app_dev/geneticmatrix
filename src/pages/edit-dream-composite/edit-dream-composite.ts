import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';
import { ChartsPage } from './../charts/charts';


@IonicPage()
@Component({
  selector: 'page-edit-dream-composite',
  templateUrl: 'edit-dream-composite.html',
})
export class EditDreamCompositePage {

  personList: any;
  selectedPerson: any;
  currentChart: any;
  chart_type: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public splash: SplashComponent, public chart: ChartProvider) {
    this.getPeopleList(this.auth.currentUser.id);
    this.currentChart = navParams.get("currentChart");
    this.chart_type = navParams.get("chart_type");
  }


  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
        console.log(this.personList);
      }
    )
  }
  public editDreamCompositChart() {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.splash.showLoading();
    this.chart.editDreamCompositChart(this.selectedPerson, this.currentChart, user_level, user_id).subscribe(
      response => {

        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart update successfully");
          // if (response == 'update') {

          this.navCtrl.setRoot(ChartsPage, { chart_id: this.currentChart, chart_type: this.chart_type });
          // }
          // else {
          //   this.splash.showError("Error while updating.")
          // }

        }
        else {
          this.splash.showError("Failed to Update chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }
}
