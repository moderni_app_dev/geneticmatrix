import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAnimalCatChartPage } from './edit-animal-cat-chart';

@NgModule({
  declarations: [
    EditAnimalCatChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAnimalCatChartPage),
  ],
})
export class EditAnimalCatChartPageModule {}
