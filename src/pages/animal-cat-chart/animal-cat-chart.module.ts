import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalCatChartPage } from './animal-cat-chart';

@NgModule({
  declarations: [
    AnimalCatChartPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalCatChartPage),
  ],
  exports: [
    AnimalCatChartPage
  ]
})
export class AnimalCatChartPageModule {}
