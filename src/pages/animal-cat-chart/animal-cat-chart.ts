import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChartsPage } from './../charts/charts';
import { AuthService } from './../../providers/auth-service';
import { SplashComponent } from './../../providers/splash-component';
import { ChartProvider } from './../../providers/chart-provider';


@IonicPage()
@Component({
  selector: 'page-animal-cat-chart',
  templateUrl: 'animal-cat-chart.html',
})
export class AnimalCatChartPage {
  countryList: any;
  animalName: string;
  birthDate: String;
  birthTime: String;
  country: String;
  countries: any;
  city: String;
  majorCity: String;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public chart: ChartProvider,
    public splash: SplashComponent,
    public auth: AuthService
  ) {
    this.getCountryList();

  }
  public getCountryList() {
    this.splash.showLoading();
    this.chart.getCountryList().subscribe(
      list => {
        this.countryList = list
        this.countries = list
      }
    )
  }

  public checkCity() {

    var dates = this.birthDate.split("-");
    var times = this.birthTime.split(":");

    this.majorCity = this.majorCity.trim();

    this.chart.checkCity(dates[0], dates[1], dates[2], times[0], times[1], this.country, this.majorCity).subscribe(
      response => {
        if (response == 1) {
          this.splash.showLoading();
          this.catFoundationChart(dates[0], dates[1], dates[2], times[0], times[1])
        }
        else {
          this.splash.showError("Nearest Major City not recognized.");
        }
      }
    );

  }

  public catFoundationChart(year, month, day, hours, minutes) {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    // this.firstName = this.firstName.trim();
    // this.lastName = this.lastName.trim();
    this.chart.createFoundationAnimalCatChart(this.animalName, user_level, user_id, year, month, day, hours, minutes, this.country, this.city, this.majorCity).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, response);
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }

  searchCountry(searchbar) {
    // reset countries list with initial call
    this.countries = this.country;
    // set q to the value of the searchbar
    var q = searchbar.value;

    // if the value is an empty string don't filter the items
    if (q.trim() == '') {
      return;
    }

    this.countries = this.countries.filter((v) => {
      if (v.toLowerCase().indexOf(q.toLowerCase()) > -1) {
        return true;
      }
      return false;
    })


  }
}
