import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditChironChartPage } from './edit-chiron-chart';

@NgModule({
  declarations: [
    EditChironChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditChironChartPage),
  ],
})
export class EditChironChartPageModule {}
