"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var home_1 = require("./../home/home");
var add_chart_1 = require("./../add-chart/add-chart");
var splash_component_1 = require("./../../providers/splash-component");
var auth_service_1 = require("./../../providers/auth-service");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var chart_provider_1 = require("../../providers/chart-provider");
var login_1 = require("../login/login");
var platform_browser_1 = require("@angular/platform-browser");
var ionic_angular_2 = require("ionic-angular");
var ChartsPage = /** @class */ (function () {
    function ChartsPage(menuCtrl, platform, navCtrl, chart, sanitizer, auth, splash, navParams) {
        this.menuCtrl = menuCtrl;
        this.platform = platform;
        this.navCtrl = navCtrl;
        this.chart = chart;
        this.sanitizer = sanitizer;
        this.auth = auth;
        this.splash = splash;
        this.navParams = navParams;
        this.menuCtrl.enable(true, 'chartsMenu');
        this.menuCtrl.enable(false, 'mainMenu');
        this.flag = 1;
        this.svgSource = '';
        this.chartList = [];
        this.chart_id = this.navParams.get('chart_id');
        this.chart_type = this.navParams.get('chart_type');
        this.getChartList(this.auth.currentUser.id);
        this.chartWidth = 100;
        this.noSearchResults = true;
        this.splitMinWidth = '768px';
        this.favourite = 0;
    }
    ChartsPage.prototype.ionViewDidLoad = function () {
    };
    ChartsPage.prototype.getChartList = function (user_id) {
        var _this = this;
        this.chart.getChartList(user_id).subscribe(function (response) {
            _this.chartList = response;
            _this.searchChartList = _this.chartList;
            if (_this.searchChartList.length != 0) {
                _this.noSearchResults = false;
            }
            if (typeof _this.chart_id === 'undefined' || typeof _this.chart_type === 'undefined') {
                _this.chartList.forEach(function (element) {
                    if (element.cs_favorite == 1) {
                        _this.getChart(element.client_id, element.cs_id, element.cs_type);
                        _this.favourite = 1;
                        _this.fav_id = element.cs_id;
                    }
                });
                if (_this.favourite == 0) {
                    _this.getChart(_this.chartList[0].client_id, _this.chartList[0].cs_id, _this.chartList[0].cs_type);
                }
            }
            else {
                _this.getChart(user_id, _this.chart_id, _this.chart_type);
            }
        });
    };
    ChartsPage.prototype.notes = function () {
        // this.chart.getNotes(this.auth.currentUser.id, this.currentChart).subscribe(
        //   response => { console.log(response) }
        // )
    };
    ChartsPage.prototype.setFavourite = function () {
        var _this = this;
        this.splash.showLoading();
        this.chart.setFavourite(this.auth.currentUser.id, this.currentChart).subscribe(function (response) {
            _this.splash.loading.dismiss();
            if (response == "1") {
                _this.splash.showSuccess("Chart set as a Favourite");
                _this.fav_id = _this.currentChart;
            }
            else {
                _this.splash.showError("There was some error. Please try again later.");
            }
        });
    };
    ChartsPage.prototype.alreadyFavourite = function () {
        this.splash.showSuccess("This chart is set as your favourite chart.");
    };
    ChartsPage.prototype.onSearchInput = function ($event) {
        this.searchChartList = Array();
        for (var i = 0; i < this.chartList.length; i++) {
            if (this.chartList[i].svc_full_name.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.chartList[i].cs_name.toLowerCase().search(this.myInput.toLowerCase()) != -1) {
                this.noSearchResults = false;
                this.searchChartList.push(this.chartList[i]);
            }
        }
        if (this.searchChartList.length == 0) {
            this.noSearchResults = true;
        }
    };
    ChartsPage.prototype.getChart = function (client_id, chart_id, chart_type) {
        var _this = this;
        this.chartWidth = 100;
        this.currentChart = chart_id;
        this.splash.showLoading();
        this.menuCtrl.close();
        this.chart.getChartOptions(chart_id, this.auth.currentUser.level).subscribe(function (response) {
            _this.chartOptions = response;
        });
        if (chart_type == 'freeChiron' || chart_type == 'freeUranus') {
            chart_type = 'freeCycle';
        }
        if (chart_type == 'freeMars' || chart_type == 'freeSolar') {
            chart_type = 'freeCycle';
        }
        if (chart_type == 'freeLunar' || chart_type == 'freeVenus' || chart_type == 'freeJupiter') {
            chart_type = 'freeCycle';
        }
        if (chart_type == 'freeSaturn' || chart_type == 'freeSaturn2' || chart_type == 'freeSaturn3') {
            chart_type = 'freeCycle';
        }
        this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=" + chart_id + "&type=" + chart_type + "&lvl=2";
    };
    ChartsPage.prototype.chartOptionChange = function (chart_id, chart_version, chart_type, chart_level) {
        var _this = this;
        this.chartWidth = 100;
        this.splash.showLoading();
        this.menuCtrl.close();
        this.chart.getChartOptions(chart_id, this.auth.currentUser.level).subscribe(function (response) {
            _this.chartOptions = response;
        });
        this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=" + chart_id + "&ver=" + chart_version + "&type=" + chart_type + "&lvl=" + chart_level;
    };
    ChartsPage.prototype.addChart = function () {
        this.navCtrl.push(add_chart_1.AddChartPage);
    };
    ChartsPage.prototype.zoomIn = function () {
        if (this.chartWidth < 250) {
            this.chartWidth = this.chartWidth + 10;
        }
    };
    ChartsPage.prototype.zoomOut = function () {
        if (this.chartWidth > 100) {
            this.chartWidth = this.chartWidth - 10;
        }
    };
    ChartsPage.prototype.resize = function () {
        if (this.flag == 1) {
            this.flag = 0;
        }
        else {
            this.flag = 1;
        }
    };
    ChartsPage.prototype.setSplitPane = function () {
        if (this.flag == 1) {
            return false;
        }
        else {
            return true;
        }
    };
    ChartsPage.prototype.loadHomePage = function () {
        this.navCtrl.setRoot(home_1.HomePage);
    };
    ChartsPage.prototype.logout = function () {
        this.navCtrl.setRoot(login_1.LoginPage);
    };
    ChartsPage = __decorate([
        core_1.Component({
            selector: 'page-charts',
            templateUrl: 'charts.html',
        }),
        __metadata("design:paramtypes", [Object, Object, Object, chart_provider_1.ChartProvider, Object, auth_service_1.AuthService, splash_component_1.SplashComponent, Object])
    ], ChartsPage);
    return ChartsPage;
}());
exports.ChartsPage = ChartsPage;
//# sourceMappingURL=charts.js.map