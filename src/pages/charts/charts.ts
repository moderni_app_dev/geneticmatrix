import { HomePage } from './../home/home';
import { ElementRef } from '@angular/core';
import { AddChartPage } from './../add-chart/add-chart';
import { SafePipe } from './../../providers/safepipe';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService, User } from './../../providers/auth-service';
import { Component, ViewChild } from '@angular/core';
import { Platform, NavController, NavParams, AlertController } from 'ionic-angular';
import { ChartProvider } from '../../providers/chart-provider';
import { LoginPage } from '../login/login';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';
import { MenuController } from 'ionic-angular';

import { EditDreamPage } from './../edit-dream/edit-dream';
import { EditDreamTransitPage } from './../edit-dream-transit/edit-dream-transit';
import { EditDreamCompositePage } from './../edit-dream-composite/edit-dream-composite';
import { EditConnectionPage } from './../edit-connection/edit-connection';
import { EditBusinessChartPage } from './../edit-business-chart/edit-business-chart';
import { EditFamilyChartPage } from './../edit-family-chart/edit-family-chart';
import { EditLunarChartPage } from './../edit-lunar-chart/edit-lunar-chart';
import { EditSolarChartPage } from './../edit-solar-chart/edit-solar-chart';
import { EditVenusChartPage } from './../edit-venus-chart/edit-venus-chart';
import { EditMarsChartPage } from './../edit-mars-chart/edit-mars-chart';
import { EditJupiterChartPage } from './../edit-jupiter-chart/edit-jupiter-chart';
import { EditSaturnChartPage } from './../edit-saturn-chart/edit-saturn-chart';
import { EditChironChartPage } from './../edit-chiron-chart/edit-chiron-chart';
import { EditUranusOppositionChartPage } from './../edit-uranus-opposition-chart/edit-uranus-opposition-chart';
import { EditUranusChartPage } from './../edit-uranus-chart/edit-uranus-chart';
import { EditAnimalCatChartPage } from './../edit-animal-cat-chart/edit-animal-cat-chart';
import { EditAnimalDogChartPage } from './../edit-animal-dog-chart/edit-animal-dog-chart';
import { EditAnimalHorseChartPage } from './../edit-animal-horse-chart/edit-animal-horse-chart';
import { EditAnimalRabbitChartPage } from './../edit-animal-rabbit-chart/edit-animal-rabbit-chart';

import { EditAnimalTransitDogChartPage } from '../edit-animal-transit-dog-chart/edit-animal-transit-dog-chart';
import { EditAnimalTransitHorseChartPage } from '../edit-animal-transit-horse-chart/edit-animal-transit-horse-chart';
import { EditAnimalTransitRabbitChartPage } from '../edit-animal-transit-rabbit-chart/edit-animal-transit-rabbit-chart';
import { EditAnimalTransitCatChartPage } from '../edit-animal-transit-cat-chart/edit-animal-transit-cat-chart';
import { EditFoundationChartPage } from '../edit-foundation-chart/edit-foundation-chart';
@Component({
  selector: 'page-charts',
  templateUrl: 'charts.html',
})
export class ChartsPage {
  currentUserLevel: any;
  currentUser: this;
  level: this;

  svgSource: SafeResourceUrl;
  chartList: any;
  myInput: any;
  searchChartList: any;
  noSearchResults: any;


  flag: number;
  chartOptions: any;
  currentChart: any;

  chart_id: any;
  chart_type: any;
  baseWidth: any;
  chartWidth: any;
  splitMinWidth: any;

  favourite: any;
  fav_id: any;
  zoomLevel: any;

  constructor(private alertCtrl: AlertController, public menuCtrl: MenuController, public platform: Platform, public navCtrl: NavController, public chart: ChartProvider, public sanitizer: DomSanitizer, public auth: AuthService, public splash: SplashComponent, public navParams: NavParams) {


    this.menuCtrl.enable(true, 'chartsMenu');
    this.menuCtrl.enable(false, 'mainMenu');
    this.flag = 1;
    this.svgSource = '';
    this.chartList = [];

    this.chart_id = this.navParams.get('chart_id');
    this.chart_type = this.navParams.get('chart_type');

    this.currentUserLevel = this.auth.currentUser.level;

    this.getChartList(this.auth.currentUser.id);

    this.chartWidth = 100;
    this.noSearchResults = true;
    this.splitMinWidth = '768px';

    this.favourite = 0;

  }


  public getChartList(user_id) {

    this.chart.getChartList(user_id).subscribe(
      response => {

        this.chartList = response;

        this.searchChartList = this.chartList;
        if (this.searchChartList.length != 0) {
          this.noSearchResults = false;
        }
        if (typeof this.chart_id === 'undefined' || typeof this.chart_type === 'undefined') {

          this.chartList.forEach(element => {

            if (element.cs_favorite == 1) {
              this.getChart(element.client_id, element.cs_id, element.cs_type);
              this.favourite = 1;
              this.fav_id = element.cs_id;
            }
          });

          if (this.favourite == 0) {
            this.getChart(this.chartList[0].client_id, this.chartList[0].cs_id, this.chartList[0].cs_type);
          }
        }
        else {
          this.getChart(user_id, this.chart_id, this.chart_type);
        }
      }
    )
  }

  public notes() {
    // this.chart.getNotes(this.auth.currentUser.id, this.currentChart).subscribe(
    //   response => { console.log(response) }
    // )
  }

  public setFavourite() {
    this.splash.showLoading();
    this.chart.setFavourite(this.auth.currentUser.id, this.currentChart).subscribe(
      response => {

        this.splash.loading.dismiss();

        if (response == "1") {
          this.splash.showSuccess("Chart set as a Favourite");
          this.fav_id = this.currentChart;
        }
        else {
          this.splash.showError("There was some error. Please try again later.");
        }
      }
    )
  }

  public alreadyFavourite() {
    this.splash.showSuccess("This chart is set as your favourite chart.");
  }

  public onSearchInput($event) {
    this.searchChartList = Array();
    for (var i = 0; i < this.chartList.length; i++) {

      if (this.chartList[i].svc_full_name.toLowerCase().search(this.myInput.toLowerCase()) != -1 || this.chartList[i].cs_name.toLowerCase().search(this.myInput.toLowerCase()) != -1) {
        this.noSearchResults = false;
        this.searchChartList.push(this.chartList[i]);
      }
    }

    if (this.searchChartList.length == 0) {
      this.noSearchResults = true;
    }
  }

  public getChart(client_id, chart_id, chart_type) {
    console.log(this.auth.currentUser.level);
    console.log(chart_type);
    if (this.currentUserLevel == 1 && chart_type != 'freeFound') {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to Advanced or Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
    else if (this.currentUserLevel > 1 && this.currentUserLevel < 3 && (chart_type == 'freeDreamC' || chart_type == 'freeFamily' || chart_type == 'freeBusiness' || chart_type == 'freeLunar' || chart_type == 'freeVenus' || chart_type == 'freeMars' || chart_type == 'freeJupiter' || chart_type == 'freeUranus2' || chart_type == 'freeAnimal')) {
      let alert = this.alertCtrl.create({
        title: 'Upgrade Account',
        subTitle: 'Please Upgrade to  Pro Member status to create this chart.',
        buttons: ['OK']
      });
      alert.present();
    }
    else {
      this.chart_type = chart_type;
      this.chartWidth = 100;
      this.currentChart = chart_id;
      this.splash.showLoading();
      this.menuCtrl.close();
      this.chart.getChartOptions(chart_id, this.auth.currentUser.level).subscribe(
        response => {
          this.chartOptions = response;
        }
      )


      if (chart_type == 'freeChiron' || chart_type == 'freeUranus') {
        chart_type = 'freeCycle';
      }
      ` `
      if (chart_type == 'freeMars' || chart_type == 'freeSolar') {
        chart_type = 'freeCycle';
      }

      if (chart_type == 'freeLunar' || chart_type == 'freeVenus' || chart_type == 'freeJupiter') {
        chart_type = 'freeCycle';
      }

      if (chart_type == 'freeSaturn' || chart_type == 'freeSaturn2' || chart_type == 'freeSaturn3') {
        chart_type = 'freeCycle';
      }

      this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=" + chart_id + "&type=" + chart_type + "&lvl=2";
    }
  }

  public chartOptionChange(chart_id, chart_version, chart_type, chart_level) {
    this.chartWidth = 100;
    this.splash.showLoading();
    this.menuCtrl.close();
    this.chart.getChartOptions(chart_id, this.auth.currentUser.level).subscribe(
      response => {
        this.chartOptions = response;
      }
    )
    this.svgSource = "http://www.geneticmatrixtest.com/wp-content/themes/twentyten/assets/chart_svg.php?chart_id=" + chart_id + "&ver=" + chart_version + "&type=" + chart_type + "&lvl=" + chart_level;
  }

  public addChart() {
    this.navCtrl.push(AddChartPage);
  }


  public zoomIn() {
    if (this.chartWidth < 250) {
      this.chartWidth = this.chartWidth + 10;
    }
  }

  public zoomOut() {
    if (this.chartWidth > 100) {
      this.chartWidth = this.chartWidth - 10;
    }
  }

  public resize() {
    if (this.flag == 1) {
      this.flag = 0;
    }
    else {
      this.flag = 1
    }
  }

  public setSplitPane() {
    if (this.flag == 1) {
      return false;
    }
    else {
      return true;
    }
  }

  public loadHomePage() {
    this.navCtrl.setRoot(HomePage);
  }

  public logout() {
    let alert = this.alertCtrl.create({
      title: 'Logout',
      message: 'Are you sure you want to logout?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Logout',
          handler: () => {
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();

  }

  public editChart() {
    console.log(this.chart_id);
    console.log(this.chart_type);
    console.log(this.currentChart);
    console.log(this.auth.currentUser.id);
    console.log(this.auth.currentUser.level);

    if (this.chart_type == 'freeDream') {
      this.navCtrl.push(EditDreamPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeDreamT') {
      this.navCtrl.push(EditDreamTransitPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeDreamC') {
      this.navCtrl.push(EditDreamCompositePage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeConn') {
      this.navCtrl.push(EditConnectionPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeBusiness') {
      this.navCtrl.push(EditBusinessChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeFamily') {
      this.navCtrl.push(EditFamilyChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeLunar') {
      this.navCtrl.push(EditLunarChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeSolar') {
      this.navCtrl.push(EditSolarChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeVenus') {
      this.navCtrl.push(EditVenusChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeMars') {
      this.navCtrl.push(EditMarsChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeJupiter') {
      this.navCtrl.push(EditJupiterChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeSaturn' || this.chart_type == 'freeSaturn2' || this.chart_type == 'freeSaturn3') {
      this.navCtrl.push(EditSaturnChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeChiron') {
      this.navCtrl.push(EditChironChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeUranus') {
      this.navCtrl.push(EditUranusOppositionChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeUranus2') {
      this.navCtrl.push(EditUranusChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      });
    }
    if (this.chart_type == 'freeAnimal') {
      this.chart.getChartTypeAnimal(this.currentChart).subscribe(
        response => {
          if (response == 'c') {
            this.navCtrl.push(EditAnimalCatChartPage, {
              currentChart: this.currentChart,
              chart_type: this.chart_type,
            });
          }

          if (response == 'd') {
            this.navCtrl.push(EditAnimalDogChartPage, {
              currentChart: this.currentChart,
              chart_type: this.chart_type,
            });
          }
          if (response == 'h') {
            this.navCtrl.push(EditAnimalHorseChartPage, {
              currentChart: this.currentChart,
              chart_type: this.chart_type,
            });
          }
          if (response == 'r') {
            this.navCtrl.push(EditAnimalRabbitChartPage, {
              currentChart: this.currentChart,
              chart_type: this.chart_type,
            });
          }
        })

    }

    if (this.chart_type == 'freeAnimalT') {
      this.chart.getChartTypeAnimal(this.currentChart).subscribe(
        response => {
          if (response == 'r') {
            this.navCtrl.push(EditAnimalTransitRabbitChartPage, {
              currentChart: this.currentChart,
              chart_type: this.chart_type,
            });
          }
          if (response == 'd') {
            this.navCtrl.push(EditAnimalTransitDogChartPage, {
              currentChart: this.currentChart,
              chart_type: this.chart_type,
            });
          }
          if (response == 'c') {
            this.navCtrl.push(EditAnimalTransitCatChartPage, {
              currentChart: this.currentChart,
              chart_type: this.chart_type,
            });
          }
          if (response == 'h') {
            this.navCtrl.push(EditAnimalTransitHorseChartPage, {
              currentChart: this.currentChart,
              chart_type: this.chart_type,
            });
          }

        })
    }
    if (this.chart_type == 'freeFound') {
      this.navCtrl.push(EditFoundationChartPage, {
        currentChart: this.currentChart,
        chart_type: this.chart_type,
      })
    }

  }
}