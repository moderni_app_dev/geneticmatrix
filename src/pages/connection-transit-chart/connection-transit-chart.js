"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var charts_1 = require("./../charts/charts");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var chart_provider_1 = require("./../../providers/chart-provider");
var splash_component_1 = require("./../../providers/splash-component");
var auth_service_1 = require("./../../providers/auth-service");
var ConnectionTransitChartPage = /** @class */ (function () {
    function ConnectionTransitChartPage(navCtrl, navParams, auth, splash, chart, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.splash = splash;
        this.chart = chart;
        this.toastCtrl = toastCtrl;
        this.required = 'required';
        this.getPeopleList(this.auth.currentUser.id);
    }
    ConnectionTransitChartPage.prototype.getPeopleList = function (user_id) {
        var _this = this;
        this.splash.showLoading();
        this.chart.getPeopleList(this.auth.currentUser.id).subscribe(function (response) {
            _this.personList = response;
        });
    };
    ConnectionTransitChartPage.prototype.checkTransitNow = function () {
        var user_level = this.auth.currentUser.level;
        var user_id = this.auth.currentUser.id;
        var dates = this.birthDate.split("-");
        var times = this.birthTime.split(":");
        this.splash.showLoading();
        if (this.transitNow == true) {
            this.required = '';
            this.createConnectionTransitChart(this.selectedPerson, user_level, user_id, 1, 0, 0, 0, 0, 0);
        }
        else {
            this.createConnectionTransitChart(this.selectedPerson, user_level, user_id, 0, dates[0], dates[1], dates[2], times[0], times[1]);
        }
    };
    ConnectionTransitChartPage.prototype.createConnectionTransitChart = function (selectedPerson, user_level, user_id, transitNow, year, month, day, hour, minutes) {
        var _this = this;
        if (this.selectedPerson.length != 2) {
            var toast = this.toastCtrl.create({
                message: 'Select 2 names',
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
        else {
            this.chart.createConnectionTransitChart(selectedPerson, user_level, user_id, transitNow, year, month, day, hour, minutes).subscribe(function (response) {
                if (response != false) {
                    _this.splash.loading.dismiss();
                    _this.splash.showSuccess("Chart Created successfully");
                    _this.navCtrl.setRoot(charts_1.ChartsPage, response);
                }
                else {
                    _this.splash.showError("Failed to create chart. Make sure you are connected to internet.");
                }
            });
        }
    };
    ConnectionTransitChartPage.prototype.cancelCreate = function () {
        this.navCtrl.pop();
    };
    ConnectionTransitChartPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad ConnectionTransitChartPage');
    };
    ConnectionTransitChartPage = __decorate([
        ionic_angular_1.IonicPage(),
        core_1.Component({
            templateUrl: 'connection-transit-chart.html',
        }),
        __metadata("design:paramtypes", [Object, Object, auth_service_1.AuthService,
            splash_component_1.SplashComponent,
            chart_provider_1.ChartProvider, Object])
    ], ConnectionTransitChartPage);
    return ConnectionTransitChartPage;
}());
exports.ConnectionTransitChartPage = ConnectionTransitChartPage;
//# sourceMappingURL=connection-transit-chart.js.map