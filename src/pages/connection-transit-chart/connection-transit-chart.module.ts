import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConnectionTransitChartPage } from './connection-transit-chart';

@NgModule({
  declarations: [
    ConnectionTransitChartPage,
  ],
  imports: [
    IonicPageModule.forChild(ConnectionTransitChartPage),
  ],
  exports: [
    ConnectionTransitChartPage
  ]
})
export class ConnectionTransitChartPageModule {}
