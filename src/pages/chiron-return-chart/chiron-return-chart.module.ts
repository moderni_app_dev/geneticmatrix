import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChironReturnChartPage } from './chiron-return-chart';

@NgModule({
  declarations: [
    ChironReturnChartPage,
  ],
  imports: [
    IonicPageModule.forChild(ChironReturnChartPage),
  ],
  exports: [
    ChironReturnChartPage
  ]
})
export class ChironReturnChartPageModule {}
