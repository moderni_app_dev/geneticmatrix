import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditJupiterChartPage } from './edit-jupiter-chart';

@NgModule({
  declarations: [
    EditJupiterChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditJupiterChartPage),
  ],
})
export class EditJupiterChartPageModule {}
