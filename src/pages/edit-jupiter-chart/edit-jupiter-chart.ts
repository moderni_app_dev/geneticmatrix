import { ChartsPage } from './../charts/charts';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HomePage } from './../home/home';
import { DefineProvider } from './../../providers/define';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';


@IonicPage()
@Component({
  selector: 'page-edit-jupiter-chart',
  templateUrl: 'edit-jupiter-chart.html',
})
export class EditJupiterChartPage {
  personList: any;
  selectedPerson: any;
  returnCycle: any;
  currentChart: any;
  chart_type: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public splash: SplashComponent, public chart: ChartProvider, public define: DefineProvider) {
    this.getPeopleList(this.auth.currentUser.id);
    this.currentChart = navParams.get("currentChart");
    this.chart_type = navParams.get("chart_type");

  }

  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }

  public editJupiterReturnChart() {
    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.splash.showLoading();
    this.chart.editJupiterReturnChart(this.selectedPerson, this.currentChart, user_level, user_id, this.returnCycle).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Update successfully");
          console.log(this.currentChart);
          console.log(this.chart_type);
          this.navCtrl.setRoot(ChartsPage, { chart_id: this.currentChart, chart_type: this.chart_type });
        }
        else {
          this.splash.showError("Failed to update chart. Make sure you are connected to internet.");
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }

}
