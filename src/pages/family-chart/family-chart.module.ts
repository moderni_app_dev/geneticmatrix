import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FamilyChartPage } from './family-chart';

@NgModule({
  declarations: [
    FamilyChartPage,
  ],
  imports: [
    IonicPageModule.forChild(FamilyChartPage),
  ],
  exports: [
    FamilyChartPage
  ]
})
export class FamilyChartPageModule {}
