"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var charts_1 = require("./../charts/charts");
var define_1 = require("./../../providers/define");
var chart_provider_1 = require("./../../providers/chart-provider");
var splash_component_1 = require("./../../providers/splash-component");
var auth_service_1 = require("./../../providers/auth-service");
var FamilyChartPage = /** @class */ (function () {
    function FamilyChartPage(navCtrl, navParams, auth, splash, chart, define, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.auth = auth;
        this.splash = splash;
        this.chart = chart;
        this.define = define;
        this.toastCtrl = toastCtrl;
        this.getPeopleList(this.auth.currentUser.id);
    }
    FamilyChartPage.prototype.getPeopleList = function (user_id) {
        var _this = this;
        this.splash.showLoading();
        this.chart.getPeopleList(this.auth.currentUser.id).subscribe(function (response) {
            _this.personList = response;
        });
    };
    FamilyChartPage.prototype.familyChart = function () {
        var _this = this;
        var client_id = this.auth.currentUser.id;
        var user_level = this.auth.currentUser.level;
        if (this.selectedPerson.length < 3 || this.selectedPerson.length > 5) {
            var toast = this.toastCtrl.create({
                message: 'select minimum 3 and maximum 5 names',
                duration: 3000,
                position: 'top'
            });
            toast.present();
        }
        else {
            this.chart.createFamilyChart(this.selectedPerson, user_level, client_id, this.chartName)
                .subscribe(function (response) {
                if (response != false) {
                    _this.splash.loading.dismiss();
                    _this.splash.showSuccess("Chart Created successfully");
                    _this.navCtrl.setRoot(charts_1.ChartsPage, response);
                }
                else {
                    _this.splash.showError("Failed to create chart. Make sure you are connected to internet.");
                }
            });
        }
    };
    FamilyChartPage.prototype.cancelCreate = function () {
        this.navCtrl.pop();
    };
    FamilyChartPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad FamilyChartPage');
    };
    FamilyChartPage = __decorate([
        ionic_angular_1.IonicPage(),
        core_1.Component({
            selector: 'page-family-chart',
            templateUrl: 'family-chart.html',
        }),
        __metadata("design:paramtypes", [Object, Object, auth_service_1.AuthService,
            splash_component_1.SplashComponent,
            chart_provider_1.ChartProvider,
            define_1.DefineProvider, Object])
    ], FamilyChartPage);
    return FamilyChartPage;
}());
exports.FamilyChartPage = FamilyChartPage;
//# sourceMappingURL=family-chart.js.map