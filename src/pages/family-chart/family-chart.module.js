"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var family_chart_1 = require("./family-chart");
var FamilyChartPageModule = /** @class */ (function () {
    function FamilyChartPageModule() {
    }
    FamilyChartPageModule = __decorate([
        core_1.NgModule({
            declarations: [
                family_chart_1.FamilyChartPage,
            ],
            imports: [
                ionic_angular_1.IonicPageModule.forChild(family_chart_1.FamilyChartPage),
            ],
            exports: [
                family_chart_1.FamilyChartPage
            ]
        })
    ], FamilyChartPageModule);
    return FamilyChartPageModule;
}());
exports.FamilyChartPageModule = FamilyChartPageModule;
//# sourceMappingURL=family-chart.module.js.map