import { ChartsPage } from './../charts/charts';
import { HomePage } from './../home/home';
import { AuthService } from './../../providers/auth-service';
import { SplashComponent } from './../../providers/splash-component';
import { ChartProvider } from './../../providers/chart-provider';
import { LocationProvider } from '../../providers/location/location';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { NgZone, ChangeDetectorRef, ApplicationRef, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/observable/fromEvent';
import { FormControl, Validators } from '@angular/forms/';
import { SelectSearchable } from '../../components/select/select';

@Component({
  selector: 'page-foundation-chart',
  templateUrl: 'foundation-chart.html',

})
export class FoundationChartPage {

  town = "";
  @ViewChild('input') inputref: ElementRef;
  countryList: any;
  firstName: String;
  lastName: String;
  birthDate: String;
  birthTime: String;
  code: any;
  city: String = "";
  state = "";
  majorCity: String;
  globalTimeout = null;
  townControl = new FormControl('', Validators.required);
  stateControl = new FormControl('', [Validators.required, Validators.minLength(4)]);
  lat: any;
  lang: any;
  state_code_short: any;
  country_code: any;
  utc: any;
  j: any;
  port8Control: FormControl;
  constructor(private toastCtrl: ToastController, public ngzone: NgZone, public cd_ref: ChangeDetectorRef, app_ref: ApplicationRef, public navCtrl: NavController, public navParams: NavParams, public chart: ChartProvider, public splash: SplashComponent, public auth: AuthService,
    public location: LocationProvider, private alertCtrl: AlertController, ) {
    this.getCountryList();
  }

  public ngAfterViewInit() {
    this.ngzone.runOutsideAngular(() => {
      this.townControl.valueChanges.debounceTime(2000)
        .subscribe(newValue => {
          this.utc = "";
          this.state = "";
          this.change();
        });

      this.stateControl.valueChanges.debounceTime(3000)
        .subscribe(newValue => {
          this.utc = "";
          this.change_state();
        });

    });
  }
  public getCountryList() {
    this.splash.showLoading();
    this.chart.getCountryList().subscribe(
      list => {
        this.countryList = list
      }
    )
  }
  public change_state() {
    // this.utc = "";
    // this.state = "";
    this.splash.showLoading();
    this.geo();
    this.utcTime();

  }

  public change_country() {

    if (this.birthDate != undefined && this.birthTime != undefined && this.city != "") {
      // this.splash.showLoading(); 
      this.geo();
      // this.utcTime();

    }
  }
  public change() {

    if (this.birthDate != undefined && this.birthTime != undefined && this.city != "") {
      // this.splash.showLoading();
      this.geo();
      // this.utcTime();
    }

  }
  public timeDateChange() {
    if (this.birthDate != undefined && this.birthTime != undefined && this.city != "") {
      this.splash.showLoading();
      this.geo();
      this.utcTime();
    }
  }
  public geo() {
    // this.utc = "";
    var state_code = '';
    var state_code_short = '';
    var country_code = '';
    this.location.getlocation(this.city, this.state, this.countryList[this.code].country_title, this.countryList[this.code].code).subscribe(response => {

      var lat = response.results[0].geometry.location.lat;
      var lang = response.results[0].geometry.location.lng;

      this.lang = lang;
      this.lat = lat;
      var addrComponents = response.results[0].address_components;
      for (var i = 0; i < addrComponents.length; i++) {
        //console.log(addrComponents);
        if (addrComponents[i].types[0] == 'administrative_area_level_1') {
          state_code = addrComponents[i].long_name;
          state_code_short = addrComponents[i].short_name;
        }
        if (addrComponents[i].types[0] == "country") {
          country_code = addrComponents[i].short_name;

        }
        if (addrComponents[i].types.length == 2) {
          if (addrComponents[i].types[0] == "political") {
            country_code = addrComponents[i].short_name;

          }
          if (addrComponents[i].types[0] == 'administrative_area_level_2') {
            state_code = addrComponents[i].long_name;
            state_code_short = addrComponents[i].short_name;
          } else if (addrComponents[i].types[0] == 'locality') {
            state_code = addrComponents[i].long_name;
            state_code_short = addrComponents[i].short_name;
          }
        }
        if (addrComponents.length == 3) {
          if (addrComponents[i].types[0] == "country") {
            country_code = addrComponents[i].short_name;

          }
          if (addrComponents[i].types[0] == 'administrative_area_level_3') {
            state_code = addrComponents[i].long_name;
            state_code_short = addrComponents[i].short_name;
          }
        }
      }
      if (addrComponents.length == 1) {
        country_code = '01';
      }
      else {
        lat = '';
        lang = '';
      }
      this.state = state_code;
      if (this.city != "" && state_code == '') {
        this.utc = "";
        let alert = this.alertCtrl.create({
          title: 'Invalid City',
          subTitle: 'City Entered is not Valid..!!',
          buttons: ['OK']
        });
        alert.present();
        this.city = "";

      }
      // else{
      //   this.splash.loading.dismiss();
      // }
      this.country_code = country_code;
      this.state_code_short = state_code_short;
      if (this.birthDate != undefined && this.birthTime != undefined && this.city != "") {
      }
      // this.splash.loading.dismiss();
      // this.splash.showSuccess("Chart Created successfully");
    })

  }
  public utcTime() {
    if (this.birthDate != undefined && this.birthTime != undefined && this.city != "" && this.state != "") {
      this.location.utcTime(this.birthDate, this.birthTime, this.lat, this.lang, this.state_code_short, this.country_code, this.countryList[this.code].code).subscribe(
        response => {
          this.utc = response.utc_time;
        });

    }

  }
  public checkCity() {
    var dates = this.birthDate.split("-");
    var times = this.birthTime.split(":");
    var utc = this.utc.split(" ");
    var utcTime = utc[1].split(":");


    // this.majorCity = this.majorCity.trim();

    // this.chart.checkCity(dates[0], dates[1], dates[2], times[0], times[1], this.code, this.majorCity).subscribe(
    //   response => {
    //     if (response == 1) {
    this.splash.showLoading();
    // if (this.birthDate != undefined && this.birthTime != undefined && this.city != "" && this.state != "" && this.utc != undefined) {
    this.createFoundationChart(dates[0], dates[1], dates[2], times[0], times[1], utc[0], utcTime[0], utcTime[1]);
    // }
    // }
    //     else {
    //       this.splash.showError("Nearest Major City not recognized.");
    //     }
    //   }
    // );
  }

  public createFoundationChart(year, month, day, hours, minutes, utcDate, utcHour, utcMin) {


    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.firstName = this.firstName.trim();
    this.lastName = this.lastName.trim();
    this.chart.createFoundationChart(this.firstName, this.lastName, year, month, day, hours, minutes, this.country_code, this.state, this.state_code_short, this.city, utcDate, utcHour, utcMin, user_id, user_level).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, response);
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }

  public cancelCreate() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    // console.log('ionViewDidLoad FundationChartPage');
  }
  searchPorts(event: { component: SelectSearchable, text: string }) {
    let text = (event.text || '').trim().toLowerCase();

    if (!text) {
      event.component.items = [];
      return;
    } else if (event.text.length < 3) {
      return;
    }

    event.component.isSearching = true;

    // Simulate AJAX.
    setTimeout(() => {
      event.component.items = this.countryList().filter(port => {
        return port.country_code.toLowerCase().indexOf(text) !== -1 ||
          port.country_title.toLowerCase().indexOf(text) !== -1;
      });

      event.component.isSearching = false;
    }, 2000);
  }

  portTemplate(port: any) {
    return `${port.country_code} (${port.country_title})`;
  }

  portChange(event: { component: SelectSearchable, value: any }) {
    console.log('value:', event.value);
  }

  reset() {
    this.port8Control.reset();
  }

}
