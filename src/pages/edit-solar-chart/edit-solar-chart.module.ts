import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditSolarChartPage } from './edit-solar-chart';

@NgModule({
  declarations: [
    EditSolarChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditSolarChartPage),
  ],
})
export class EditSolarChartPageModule {}
