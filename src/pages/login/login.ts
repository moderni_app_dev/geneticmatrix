import { MainHomePage } from './../main-home/main-home';
import { AddChartPage } from './../add-chart/add-chart';
import { ForgetPasswordPage } from './../forget-password/forget-password';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SplashComponent } from '../../providers/splash-component';
import { ChartsPage } from './../charts/charts';
import { HomePage } from '../home/home';
import { AuthService } from '../../providers/auth-service';
import { FoundationChartPage } from '../foundation-chart/foundation-chart'
import { CreateAccountPage } from '../create-account/create-account';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion';




@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})

export class LoginPage {
  msg: any;
  acceleration: any;
  acceleration1: any;
  loginCredentials = { username: '', password: '' };
  constructor(public navCtrl: NavController, public navParams: NavParams, public splash: SplashComponent, public auth: AuthService) {
    // this.loginCredentials = { username: '', password: '' };
    // this.loginCredentials={username:'jytest',password:'jytest'};
    // this.loginCredentials={username:'shyamtest',password:'moderni@123'};
    this.loginCredentials = { username: 'shyam', password: 'shyam' };
    this.login();
    // this.create_account();
    // this.msg = this.navParams.get('msg');
    // alert(this.screenOrientation.type);
    // this.screenOrientation.onChange().subscribe(
    //   () => {
    //     alert("Orientation Changed");
    //   }
    // );
    // this.deviceMotion.getCurrentAcceleration().then(
    //   (acceleration: DeviceMotionAccelerationData) => this.acceleration = acceleration,
    //   (error: any) => console.log(error)

    // );

    // Watch device acceleration
    // var subscription = this.deviceMotion.watchAcceleration().subscribe((acceleration1: DeviceMotionAccelerationData) => {
    //   console.log(acceleration1);
    //   this.acceleration1 = acceleration1
    // });
  }


  public login() {

    //show loading animation
    this.splash.showLoading();

    //subscribe to authentication services to authenticate user and userdata from server 
    this.auth.login(this.loginCredentials).subscribe(response => {


      // return;
      // if server return success
      if (response.token !== '') {

        this.splash.loading.dismiss();

        //save auth token 
        this.auth.setAuthToken(response.token);

        this.auth.setUser(response);

        // this.navCtrl.setRoot(FoundationChartPage);
        // this.navCtrl.setRoot(HomePage);
        this.navCtrl.setRoot(ChartsPage);


      } else {
        // server returned fail. authentication failure. 
        this.splash.loading.dismiss();

        this.splash.showError(response.code);
      }
    },
      error => {
        // this.splash.loading.dismiss();

        // this.splash.showError(error);
      });

  }

  public forget_password() {
    this.navCtrl.push(ForgetPasswordPage);
  }

  public create_account() {
    this.navCtrl.push(CreateAccountPage);
  }
  ionViewDidLoad() {
    // console.log('ionViewDidLoad Login');
  }
}
