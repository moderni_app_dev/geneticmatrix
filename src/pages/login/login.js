"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var forget_password_1 = require("./../forget-password/forget-password");
var core_1 = require("@angular/core");
var ionic_angular_1 = require("ionic-angular");
var splash_component_1 = require("../../providers/splash-component");
var home_1 = require("../home/home");
var auth_service_1 = require("../../providers/auth-service");
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, splash, auth) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.splash = splash;
        this.auth = auth;
        this.loginCredentials = { username: '', password: '' };
        this.loginCredentials = { username: '', password: '' };
        // this.loginCredentials={username:'jytest',password:'jytest'};
        // this.loginCredentials={username:'shyamtest',password:'moderni@123'};
        this.loginCredentials = { username: 'shyam', password: 'shyam' };
        this.login();
    }
    LoginPage.prototype.login = function () {
        var _this = this;
        //show loading animation
        this.splash.showLoading();
        //subscribe to authentication services to authenticate user and userdata from server 
        this.auth.login(this.loginCredentials).subscribe(function (response) {
            // console.log(response);
            // return;
            // if server return success
            if (response.token !== '') {
                _this.splash.loading.dismiss();
                //save auth token 
                _this.auth.setAuthToken(response.token);
                _this.auth.setUser(response);
                // this.navCtrl.setRoot(FoundationChartPage);
                _this.navCtrl.setRoot(home_1.HomePage);
            }
            else {
                // server returned fail. authentication failure. 
                _this.splash.loading.dismiss();
                _this.splash.showError(response.code);
            }
        }, function (error) {
            _this.splash.loading.dismiss();
            _this.splash.showError(error);
        });
    };
    LoginPage.prototype.forget_password = function () {
        this.navCtrl.push(forget_password_1.ForgetPasswordPage);
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        // console.log('ionViewDidLoad Login');
    };
    LoginPage = __decorate([
        core_1.Component({
            selector: 'page-login',
            templateUrl: 'login.html',
        }),
        __metadata("design:paramtypes", [Object, Object, splash_component_1.SplashComponent, auth_service_1.AuthService])
    ], LoginPage);
    return LoginPage;
}());
exports.LoginPage = LoginPage;
//# sourceMappingURL=login.js.map