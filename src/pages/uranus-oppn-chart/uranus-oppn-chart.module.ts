import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UranusOppnChartPage } from './uranus-oppn-chart';

@NgModule({
  declarations: [
    UranusOppnChartPage,
  ],
  imports: [
    IonicPageModule.forChild(UranusOppnChartPage),
  ],
})
export class UranusOppnChartPageModule {}
