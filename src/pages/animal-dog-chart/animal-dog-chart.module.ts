import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalDogChartPage } from './animal-dog-chart';

@NgModule({
  declarations: [
    AnimalDogChartPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalDogChartPage),
  ],
  exports: [
    AnimalDogChartPage
  ]
})
export class AnimalDogChartPageModule {}
