import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditBusinessChartPage } from './edit-business-chart';

@NgModule({
  declarations: [
    EditBusinessChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditBusinessChartPage),
  ],
})
export class EditBusinessChartPageModule {}
