import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ChartsPage } from './../charts/charts';
import { DefineProvider } from './../../providers/define';
import { ChartProvider } from './../../providers/chart-provider';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';

@IonicPage()
@Component({
  selector: 'page-edit-business-chart',
  templateUrl: 'edit-business-chart.html',
})
export class EditBusinessChartPage {
  personList: any;
  selectedPerson: any;
  chartName: any;
  currentChart: any;
  chart_type: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public auth: AuthService,
    public splash: SplashComponent,
    public chart: ChartProvider,
    public define: DefineProvider,
    public toastCtrl: ToastController
  ) {
    this.getPeopleList(this.auth.currentUser.id);
    this.currentChart = navParams.get("currentChart");
    this.chart_type = navParams.get("chart_type");

  }
  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;
      }
    )
  }

  BusinessChart() {
    let client_id = this.auth.currentUser.id;
    var user_level = this.auth.currentUser.level;
    if (this.selectedPerson.length < 3 || this.selectedPerson.length > 5) {
      let toast = this.toastCtrl.create({
        message: 'select minimum 3 and maximum 5 names',
        duration: 3000,
        position: 'top'
      });
      toast.present();
    }
    else {

      this.chart.editBusinessChart(this.selectedPerson, this.currentChart, user_level, client_id, this.chartName)
        .subscribe(response => {
          if (response != false) {
            this.splash.loading.dismiss();
            this.splash.showSuccess("Chart Created successfully");
            this.navCtrl.setRoot(ChartsPage, { chart_id: this.currentChart, chart_type: this.chart_type });
          }
          else {
            this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
          }
        })
    }
  }
  public cancelCreate() {
    this.navCtrl.pop();
  }

}