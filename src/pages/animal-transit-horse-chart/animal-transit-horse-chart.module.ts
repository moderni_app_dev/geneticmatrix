import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AnimalTransitHorseChartPage } from './animal-transit-horse-chart';

@NgModule({
  declarations: [
    AnimalTransitHorseChartPage,
  ],
  imports: [
    IonicPageModule.forChild(AnimalTransitHorseChartPage),
  ],
  exports: [
    AnimalTransitHorseChartPage
  ]
})
export class AnimalTransitHorseChartPageModule {}
