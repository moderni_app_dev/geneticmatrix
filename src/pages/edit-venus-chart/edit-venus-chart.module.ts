import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditVenusChartPage } from './edit-venus-chart';

@NgModule({
  declarations: [
    EditVenusChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditVenusChartPage),
  ],
})
export class EditVenusChartPageModule {}
