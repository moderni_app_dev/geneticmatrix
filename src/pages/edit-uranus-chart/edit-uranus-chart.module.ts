import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditUranusChartPage } from './edit-uranus-chart';

@NgModule({
  declarations: [
    EditUranusChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditUranusChartPage),
  ],
})
export class EditUranusChartPageModule {}
