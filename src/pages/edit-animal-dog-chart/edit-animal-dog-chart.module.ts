import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAnimalDogChartPage } from './edit-animal-dog-chart';

@NgModule({
  declarations: [
    EditAnimalDogChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAnimalDogChartPage),
  ],
})
export class EditAnimalDogChartPageModule {}
