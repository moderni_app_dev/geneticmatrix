import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditUranusOppositionChartPage } from './edit-uranus-opposition-chart';

@NgModule({
  declarations: [
    EditUranusOppositionChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditUranusOppositionChartPage),
  ],
})
export class EditUranusOppositionChartPageModule {}
