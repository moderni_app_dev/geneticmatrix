import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditMarsChartPage } from './edit-mars-chart';

@NgModule({
  declarations: [
    EditMarsChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditMarsChartPage),
  ],
})
export class EditMarsChartPageModule {}
