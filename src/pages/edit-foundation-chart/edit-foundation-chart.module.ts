import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditFoundationChartPage } from './edit-foundation-chart';

@NgModule({
  declarations: [
    EditFoundationChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditFoundationChartPage),
  ],
})
export class EditFoundationChartPageModule {}
