import { ChartsPage } from './../charts/charts';
import { HomePage } from './../home/home';
import { AuthService } from './../../providers/auth-service';
import { SplashComponent } from './../../providers/splash-component';
import { ChartProvider } from './../../providers/chart-provider';
import { LocationProvider } from '../../providers/location/location';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { NgZone, ChangeDetectorRef, ApplicationRef, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';
// import 'rxjs/add/operator/ThrottleTime';
import 'rxjs/add/observable/fromEvent';
import { FormControl, Validators } from '@angular/forms/';

@IonicPage()
@Component({
  selector: 'page-edit-foundation-chart',
  templateUrl: 'edit-foundation-chart.html',
})
export class EditFoundationChartPage {
  town = "";
  @ViewChild('input') inputref: ElementRef;
  countryList: any;
  firstName: String;
  lastName: String;
  birthDate: String;
  birthTime: String;
  code: any;
  city: String;
  state = "";
  majorCity: String;
  globalTimeout = null;
  townControl = new FormControl('', Validators.required);
  stateControl = new FormControl('', [Validators.required, Validators.minLength(4)]);
  lat: any;
  lang: any;
  state_code_short: any;
  country_code: any;
  utc: any;
  j: any;
  currentChart: any;
  chart_type: any;
  chart_detail: any;
  details: any;
  countryy: any;
  country_title: any;
  countryCode: any;
  count: number = 0;
  constructor(private toastCtrl: ToastController, public ngzone: NgZone, public cd_ref: ChangeDetectorRef, app_ref: ApplicationRef, public navCtrl: NavController, public navParams: NavParams, public chart: ChartProvider, public splash: SplashComponent, public auth: AuthService,
    public location: LocationProvider, private alertCtrl: AlertController) {
    this.currentChart = navParams.get("currentChart");
    this.chart_type = navParams.get("chart_type");
    this.splash.showLoadingManual();
    // this.getCountryList();
    this.chart_detail = this.chart.getChartDetail(this.currentChart, this.chart_type).subscribe(
      response => {
        this.details = response
        this.chart.getCountryList().subscribe(
          list => {
            this.countryList = list;
            this.countryy = this.countryList.findIndex(x => x.country_title == response.country);
            this.country_title = this.countryList[this.countryy].country_title;
            this.countryCode = this.countryList[this.countryy].code;
            this.firstName = response.fname;
            this.lastName = response.lname;
            this.birthTime = response.time;
            this.birthDate = new Date(response.date).toJSON().split("T")[0];
            this.city = response.city;
            this.state = response.state;
          })

        // this.utc = response.dobUTC;

      }
    )

  }

  public change_state() {

    this.geo();
  }
  public ngAfterViewInit() {
    this.ngzone.runOutsideAngular(() => {
      this.townControl.valueChanges.debounceTime(3000)
        .subscribe(newValue => {
          this.utc = "";
          this.state = "";
          // this.splash.showLoading();
          this.change();
        });

      this.stateControl.valueChanges.debounceTime(3000)
        .subscribe(newValue => {
          this.utc = "";
          // this.splash.showLoading();
          this.change_state();
        });

    });
  }
  public change_country() {
    if (this.birthDate != undefined && this.birthTime != undefined && this.city != undefined) {
      // this.splash.showLoading();
      this.country_title = this.countryList[this.code].country_title;
      this.countryCode = this.countryList[this.code].code;
      this.geo();
    }
  }
  public change() {

    if (this.birthDate != undefined && this.birthTime != undefined && this.city != undefined) {
      if (this.count > 2) {
        this.splash.showLoading();

        this.geo();
      }

      this.count++;
    }

  }

  public geo() {

    // this.utc = "";
    var state_code = '';
    var state_code_short = '';
    var country_code = '';

    if (this.country_title != undefined && this.countryCode != undefined) {
      this.location.getlocation(this.city, this.state, this.country_title, this.countryCode).subscribe(response => {

        var lat = response.results[0].geometry.location.lat;
        var lang = response.results[0].geometry.location.lng;
        this.lang = lang;
        this.lat = lat;
        var addrComponents = response.results[0].address_components;
        for (var i = 0; i < addrComponents.length; i++) {
          //console.log(addrComponents);
          if (addrComponents[i].types[0] == 'administrative_area_level_1') {
            state_code = addrComponents[i].long_name;
            state_code_short = addrComponents[i].short_name;
          }
          if (addrComponents[i].types[0] == "country") {
            country_code = addrComponents[i].short_name;

          }
          if (addrComponents[i].types.length == 2) {
            if (addrComponents[i].types[0] == "political") {
              country_code = addrComponents[i].short_name;

            }
            if (addrComponents[i].types[0] == 'administrative_area_level_2') {
              state_code = addrComponents[i].long_name;
              state_code_short = addrComponents[i].short_name;
            } else if (addrComponents[i].types[0] == 'locality') {
              state_code = addrComponents[i].long_name;
              state_code_short = addrComponents[i].short_name;
            }
          }
          if (addrComponents.length == 3) {
            if (addrComponents[i].types[0] == "country") {
              country_code = addrComponents[i].short_name;

            }
            if (addrComponents[i].types[0] == 'administrative_area_level_3') {
              state_code = addrComponents[i].long_name;
              state_code_short = addrComponents[i].short_name;
            }
          }
        }
        if (addrComponents.length == 1) {
          country_code = '01';
        }
        else {
          lat = '';
          lang = '';
        }

        this.state = state_code;

        if (this.city != "" && state_code == '') {
          this.utc = "";
          this.state = "";
          let alert = this.alertCtrl.create({
            title: 'Invalid City',
            subTitle: 'City Entered is not Valid..!!',
            buttons: ['OK']
          });
          alert.present();
          this.city = "";

        }
        // else{
        //   this.splash.loading.dismiss();
        // }
        this.country_code = country_code;
        this.state_code_short = state_code_short;
        if (this.birthDate != undefined && this.birthTime != undefined && this.city != undefined) {
          this.utcTime();
        }
        // this.splash.loading.dismiss();
        // this.splash.showSuccess("Chart Created successfully");
      })
    }
  }
  public utcTime() {

    if (this.birthDate != undefined && this.birthTime != undefined && this.city != "" && this.state != "") {
      this.location.utcTime(this.birthDate, this.birthTime, this.lat, this.lang, this.state_code_short, this.country_code, this.countryCode).subscribe(
        response => {
          this.utc = response.utc_time;

          this.splash.loading.dismiss();
        });

      // 
    }

  }
  public checkCity() {
    var dates = this.birthDate.split("-");
    var times = this.birthTime.split(":");
    var utc = this.utc.split(" ");
    var utcTime = utc[1].split(":");

    // this.splash.showLoading();
    // if (this.birthDate != undefined && this.birthTime != undefined && this.city != "" && this.state != "" && this.utc != undefined) {
    this.editFoundationChart(dates[0], dates[1], dates[2], times[0], times[1], utc[0], utcTime[0], utcTime[1]);
    // }
    // }
    //     else {
    //       this.splash.showError("Nearest Major City not recognized.");
    //     }
    //   }
    // );
  }

  public editFoundationChart(year, month, day, hours, minutes, utcDate, utcHour, utcMin) {

    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;
    this.firstName = this.firstName.trim();
    this.lastName = this.lastName.trim();
    this.chart.editFoundationChart(this.firstName, this.lastName, year, month, day, hours, minutes, this.country_code, this.state, this.state_code_short, this.city, utcDate, utcHour, utcMin, user_id, user_level, this.currentChart).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Updated successfully");
          this.navCtrl.setRoot(ChartsPage, { chart_id: this.currentChart, chart_type: this.chart_type });
        }
        else {
          this.splash.showError("Failed to Update chart. Make sure you are connected to internet.")
        }
      }
    )
  }



  public cancelCreate() {
    this.navCtrl.pop();
  }
}