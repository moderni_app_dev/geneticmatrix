import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SplashComponent } from './../../providers/splash-component';
import { AuthService } from './../../providers/auth-service';
import { ChartProvider } from './../../providers/chart-provider';
import { ChartsPage } from './../charts/charts';


@IonicPage()
@Component({
  selector: 'page-edit-dream-transit',
  templateUrl: 'edit-dream-transit.html',
})
export class EditDreamTransitPage {
  personList: any;
  selectedPerson: any;
  transitNow: any;
  birthDate: String;
  disabled: String;
  birthTime: String;
  currentChart: any;
  chart_type: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public auth: AuthService, public splash: SplashComponent, public chart: ChartProvider) {
    this.getPeopleList(this.auth.currentUser.id);
    this.currentChart = navParams.get("currentChart");
    this.chart_type = navParams.get("chart_type");
  }
  public getPeopleList(user_id) {
    this.splash.showLoading();
    this.chart.getPeopleList(this.auth.currentUser.id).subscribe(
      response => {
        this.personList = response;

      }
    )
  }

  public checkTransitNow() {

    var user_level = this.auth.currentUser.level;
    var user_id = this.auth.currentUser.id;



    this.splash.showLoading();

    if (this.transitNow == true) {
      this.editDreamTransitChart(this.selectedPerson, user_level, user_id, 1, 0, 0, 0, 0, 0)
    }
    else {
      var dates = this.birthDate.split("-");
      var times = this.birthTime.split(":");
      this.editDreamTransitChart(this.selectedPerson, user_level, user_id, 0, dates[0], dates[1], dates[2], times[0], times[1])
    }
  }
  public editDreamTransitChart(selectedPerson, user_level, user_id, transitNow, year, month, day, hour, minutes) {
    this.chart.editDreamTransitChart(selectedPerson, this.currentChart, user_level, user_id, transitNow, year, month, day, hour, minutes).subscribe(
      response => {
        if (response != false) {
          this.splash.loading.dismiss();
          this.splash.showSuccess("Chart Created successfully");
          this.navCtrl.setRoot(ChartsPage, { chart_id: this.currentChart, chart_type: this.chart_type });
        }
        else {
          this.splash.showError("Failed to create chart. Make sure you are connected to internet.")
        }
      }
    )
  }
  public cancelCreate() {
    this.navCtrl.pop();
  }

}
