import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditDreamTransitPage } from './edit-dream-transit';

@NgModule({
  declarations: [
    EditDreamTransitPage,
  ],
  imports: [
    IonicPageModule.forChild(EditDreamTransitPage),
  ],
})
export class EditDreamTransitPageModule {}
