import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditAnimalTransitCatChartPage } from './edit-animal-transit-cat-chart';

@NgModule({
  declarations: [
    EditAnimalTransitCatChartPage,
  ],
  imports: [
    IonicPageModule.forChild(EditAnimalTransitCatChartPage),
  ],
  exports: [
    EditAnimalTransitCatChartPage
  ]
})
export class EditAnimalTransitCatChartPageModule {}
